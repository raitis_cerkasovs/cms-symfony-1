<?php
function getToolTipJs()
{
  echo '<script language="JavaScript" type="text/javascript" src="/js/prototype/js/wz_tooltip.js"></script>'."\n";
}

function getToolTip($text, $configName, $mode=0)
{
  $moduleName = sfContext::getInstance()->getModuleName();

  $tooltip = sfConfig::get('mod_'.strtolower($moduleName).'_help_'.$configName);
  $title = ucwords( str_replace('_', ' ',$configName) );
  $image = str_replace('"', "\'",image_tag('icons/help.png', 'align=absmiddle'));

  $js = "this.T_TITLE='$image Help - $title';return escape('$tooltip')";

  if($mode){
    return '<a href="javascript: void(null)" onMouseOver="'.$js.'" style="cursor: help">'.$text.'</a>';
  }

  return '<span class="help"><a href="javascript: void(null)" onMouseOver="'.$js.'">'.$text.'</a></span>';
}
?>

