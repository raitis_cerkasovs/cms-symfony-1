﻿  <?php if ($sf_user->isAuthenticated()): ?>
 
    <?php echo link_to('main page', 'articles/intro');?>&nbsp;|&nbsp;
    <?php echo link_to('menu', 'categories/list');?>&nbsp;|&nbsp;
    <?php echo link_to('outside articles', 'articlesout/list');?>&nbsp;|&nbsp;
    <?php echo link_to('users', 'sfGuardUser/index');?>&nbsp;|&nbsp;
    
    <?php echo link_to('photo gallery menu', 'galeries/list');?>&nbsp;|&nbsp;
    <?php echo link_to('photo gallery', 'galeries/listGals');?>&nbsp;|&nbsp;

    <?php echo link_to('EXIT', 'sfGuardAuth/signout');?>  

  <?php endif; ?>