﻿<div align="center" style="padding:10px;">
<?php use_helper('Object') ?>
<?php echo form_tag('articlesout/update') ?>

<?php echo object_input_hidden_tag($articlesout, 'getId') ?>

<div align="left">
<?php echo link_to('Cancel', 'articlesout/list', array('style' => 'color:brown;')) ?>&nbsp;
<?php echo link_to('Delete', 'articlesout/delete?id='.$articlesout->getId(), array('style' => 'color:brown;', 'post' => 'true&confirm=Are you sure?')) ?>
</div>
<br />

<div align="left">
Article name:&nbsp;
<?php echo object_input_tag($articlesout, 'getTitle', array (
  'size' => 90,
)) ?>
</div>
<br />
<?php echo object_textarea_tag($articlesout, 'getBody', array ('rich'=>'fck', 'id'=>'contentField', 'height'=>'300')) ?>

</form>
</div>