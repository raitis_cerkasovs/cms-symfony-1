﻿<div style="padding:10px;">

<?php use_helper('Object') ?>
<?php use_helper('Validation') ?>


<?php echo form_tag('categories/update') ?>

<?php echo object_input_hidden_tag($categories, 'getId') ?>

  <?php echo form_error('name')?>
  <?php echo object_input_tag($categories, 'getName', array ('size' => 15));?><br>
  
  <?php /*
Styling:
<br /><span style="color:brown;">Fonts</span><br>
  <?php echo select_tag('extmodule', options_for_select(array(
  'Arial, Helvetica, sans-serif'             => 'Arial, Helvetica, sans-serif',
  "'Times New Roman', Times, serif"          => "'Times New Roman', Times, serif",
  "Georgia, 'Times New Roman', Times, serif" => "Georgia, 'Times New Roman', Times, serif",
  "'Courier New', Courier, monospace"        => "'Courier New', Courier, monospace",
  "Verdana, Arial, Helvetica, sans-serif"    => "Verdana, Arial, Helvetica, sans-serif",
  "Geneva, Arial, Helvetica, sans-serif"     => "Geneva, Arial, Helvetica, sans-serif",
  'Tahoma'                                   => 'Tahoma',
  'Arial'                                   => 'Arial',
  "Franklin Gothic Demi Cond"                => "Franklin Gothic Demi Cond",
  "Myriad Pro"                               => "Myriad Pro"
), "Arial")) ?>

<br /><span style="color:brown;">Burtu izmērs</span><br>


  <?php echo select_tag('parent_id', options_for_select(array(
  '9px'  => '9',
  '10px' => '10',
  '12px' => '12',
  '14px' => '14',
  '16px' => '16',
  '18px' => '18',
  '24px' => '24',
  '36px' => '36',

), '14px')) ?>  
 
<br /><span style="color:brown;">Burtu platums</span><br>
 
  <?php echo select_tag('status', options_for_select(array(
  'bold'    => 'bold',
  'bolder'  => 'bolder',
  'lighter' => 'lighter',
  'normal'  => 'normal'
), 'normal')) ?>  
 
<br /><span style="color:brown;">Krāsa</span><br>
  <?php echo select_tag('isvert', options_for_select(array(
  '#000000'=> 'black',    
  '#FF0000'=> 'red', 
  '#00FF00'=> 'green', 
  '#0000FF'=> 'blue',  
  '#FFFF00'=> 'yellow', 
  '#00FFFF'=> 'green-blue',
  '#FF00FF'=> 'violet',
  '#C0C0C0'=> 'grey',
  '#FFFFFF'=> 'white', 
), '#000000')) ?>   
 
<br> 
  
  */?><br />
  

<?php echo submit_tag('OK') ?>
<?php if ($categories->getId()): ?>
  &nbsp;<?php echo link_to('Delete', 'categories/delete?id='.$categories->getId(), 'post=true&confirm=Are you sure?') ?>
  &nbsp;<?php echo link_to('Cancel', 'categories/list') ?>
<?php else: ?>
  &nbsp;<?php echo link_to('Cancel', 'categories/list') ?>
<?php endif; ?>
</form>
</div>