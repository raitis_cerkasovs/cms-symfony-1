<div style="padding-left:10px; padding-right:10px;">

<table>
<thead>
<tr>
  <th>Idfoto</th>
  <th>Path</th>
  <th>Idgal</th>
  <th>Idslud</th>
  <th>Showp</th>
</tr>
</thead>
<tbody>
<?php foreach ($fotos as $foto): ?>
<tr>
    <td><?php echo link_to($foto->getIdfoto(), 'foto/show?idfoto='.$foto->getIdfoto()) ?></td>
      <td><?php echo $foto->getPath() ?></td>
      <td><?php echo $foto->getIdgal() ?></td>
      <td><?php echo $foto->getIdslud() ?></td>
      <td><?php echo $foto->getShowp() ?></td>
  </tr>
<?php endforeach; ?>
</tbody>
</table>

<?php echo link_to ('create', 'foto/create') ?>
</div>