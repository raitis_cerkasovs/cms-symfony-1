﻿<?php use_helper('I18N') ?>
		<link rel="stylesheet" type="text/css" href="/js/jquery.fancybox/jquery.fancybox.css" media="screen" />
		<script type="text/javascript" src="/js/jquery.fancybox/jquery-1.3.2.min.js"></script>
		<script type="text/javascript" src="/js/jquery.fancybox/jquery.easing.1.3.js"></script>
		<script type="text/javascript" src="/js/jquery.fancybox/jquery.fancybox-1.2.1.pack.js"></script>
		<script type="text/javascript">
		$(document).ready(function() {
			$("a.popup").fancybox({
				'overlayOpacity':	0.75
				});
		});
		</script>


<style type="text/css">
.thumbnail {
float:left;
	margin: 5px;
	padding: 5px;
	padding-top: 15px;
	padding-bottom: 15px;
	width: 135px;
	height: 100px;
	text-align: center;
	
}

.thumbnail img {
	border: 20px solid #d4d0c8;}

a {
	text-decoration: none;}
</style>


<?php if (isset($audio)): ?>
<?php foreach ($audio as $a): ?>

<div style="margin-left:10px;><p"><?php echo $a->getText(); ?></p></div>

<p style="margin:10px;">
<audio controls src="/audio/<?php echo $a->getPath(); ?>">
   <a href="/audio/<?php echo $a->getPath(); ?>"></a>
</audio>
</p>

<?php endforeach; ?>
<?php endif; ?>


<?php foreach ($fotos as $foto): ?>

<div class="thumbnail">
<a class="popup" rel="group" href="/marked/<?php echo $foto->getPath(); ?>" title="<?php echo $foto->getText(); ?>">
<img src="/marked/<?php echo $foto->getPath(); ?>" width="100" alt="<?php echo 'Click to expand.';?>" /></a>
</div>

<?php endforeach;  ?>
<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>