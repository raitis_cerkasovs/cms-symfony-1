﻿<div style="padding:20px;">
<span style="font-size:10px;">*Click on a gallery to add, edit or delete the picture.</span><br><br>

<ul>
<?php foreach ($galeriess as $galeries): ?>
		<?php
		//////// disable links if have subsections
		$retg = '';
		if ($galeries->getSubgaleries1s()) $retg = 'return false;';
		////////
		?>

 <li>
 <?php echo link_to($galeries->getName(), 'foto/listFotoGal?id='.$galeries->getId(), array( 'style' => 'font-size:12px; color:brown;', 'onclick' => $retg)); ?>

 
        <?php if ($galeries->getSubgaleries1s()): ?>
	      <ul class="" style="z-index:4; margin-left:5px;">
             <?php foreach ($galeries->getSubgaleries1s() as $sub1):?> 
			   <li style="z-index:5;">
			     <?php echo link_to($sub1->getName(), 'foto/listFotoSubgal?id='.$sub1->getId(), array('style' => 'z-index:5;font-size:11px; color:brown;')); ?>
               </li>
		     <?php endforeach; ?>
         </ul>
		<?php endif; ?>
</li>

<?php endforeach; ?>

</ul>
</div>