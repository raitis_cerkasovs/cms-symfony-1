<?php

/**
 * langs actions.
 *
 * @package    letapolise
 * @subpackage langs
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2692 2006-11-15 21:03:55Z fabien $
 */
class langsActions extends sfActions
{

  
  public function executeSetLv()
  {
  $this->getUser()->setCulture('lv_LV');
  return $this->redirect('@homepage');
  }
  
  public function executeSetRu()
  {
  $this->getUser()->setCulture('ru_RU');
  return $this->redirect('articles/show?id=2');
  }
  
  public function executeSetEn()
  {
  $this->getUser()->setCulture('en_GB');
  return $this->redirect('articles/show?id=3');
  }
  
  public function executeSetGer()
  {
  $this->getUser()->setCulture('ger_GER');
  return $this->redirect('@homepage');
  }
  
  
  public function executeIndex()
  {
    $this->forward('default', 'module');
  }
}
