<?php

/**
 * sidebar actions.
 *
 * @package    vespuklubs
 * @subpackage sidebar
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2692 2006-11-15 21:03:55Z fabien $
 */
class sidebarComponents extends sfComponents
{
  public function executeDefault()
  {
	
   $this->gallery = $this->getRequestParameter('gallery');
  
   $c = new Criteria();
   $c->addAscendingOrderByColumn(CategoriesPeer::STATUS);
   $this->categoriess = CategoriesPeer::doSelect($c);
 
   $c = new Criteria();
   $c->addAscendingOrderByColumn(GaleriesPeer::POSITION);
   $this->galeriess = GaleriesPeer::doSelect($c);  
   
   $this->id = $this->getRequestParameter('id');
   $this->id2 = $this->getRequestParameter('id2');
  }
  

}
 