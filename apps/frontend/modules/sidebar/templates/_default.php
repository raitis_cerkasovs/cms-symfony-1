﻿<?php ///////////////////////////////////////////////////////////////////  SLOT ?>
<?php slot('anything');?>
  <?php /// something  ?>
<?php end_slot() ?>


<?php ///////////////////////////////////////////////////////////////////  EDIT OR SHOW ?>
<?php 
if ($sf_user->isAuthenticated()):
  if ($sf_user->hasCredential('moderator')):  
    $action = 'edit';
  endif;
else:
  $action = 'show';
endif;
?>


<?php ///////////////////////////////////////////////////////////////////  MENU ?>
<ul class="menu">

  <?php foreach ($categoriess as $categories): ?>
    <?php if ($sf_user->getCulture() == $categories->getLangId()):?>

      <?php /////////// styling 
      $styling = '';

      if ($categories->getExtmodule())
        $styling = 'font-family:'.$categories->getExtmodule().';';
	
      if ($categories->getParentId())
        $styling = $styling.'font-size:'.$categories->getParentId().'px;';
	
      if ($categories->getStatus())
        $styling = $styling.'font-weight:'.$categories->getStatus().';';
	
      if ($categories->getIsvert())
        $styling = $styling.'color:'.$categories->getIsvert().';';
      ?>


	  <?php /////////// disable links if have subsections
      $ret = '';
      if ($categories->getSubcategories1s()) $ret = 'return false;'
	  ?>

    
      <?php /////////// links ?>
      <li>
        <?php echo link_to($categories->getName(), 'articles/'.$action.'?id='.$categories->getIdArticles(), array( 'class' => '', 'onclick' => $ret)); ?>
 
        <?php /////////// sub-links ?>
        <?php if ($categories->getSubcategories1s()): ?>
        
	      <ul class="submenu">
             <?php foreach ($categories->getSubcategories1s() as $sub1):?> 
			   <li style="z-index:5;">
			     <?php echo link_to($sub1->getName(),'articles/'.$action.'?id='.$sub1->getIdArticles(), array('style' => 'z-index:5;')); ?>
               </li>
		     <?php endforeach; ?>
         </ul>
         
		<?php endif; ?>
      </li>
		
      <?php endif; ?>
    <?php endforeach; ?>
</ul>



<?php /////////////////////////////////////////////////////////////////// PHOTOGALERY MENU ?>
<br><br><br>

<div id="larchives">

  <span style="color:black;"> GALLERIES</span>  
           
<ul>               
   <?php foreach ($galeriess as $galeries): ?>
     <li>
        <?php echo link_to($galeries->getName(), '#', array( 'class' => '')); ?>
        <?php if ($galeries->getSubgaleries1s()): ?>
            <ul>
              <?php foreach ($galeries->getSubgaleries1s() as $sub1):?> 
			     <li>
			       <?php echo link_to($sub1->getName(), 
								    'foto/showFotosSubgal?gallery=true&id='.$sub1->getId().'&id2='.$sub1->getIdGaleries(), 
									array('style' => 'z-index:5; font-size:10px; color:brown; padding-left:25px; border:none;')); ?>
                 </li>
		      <?php endforeach; ?>
            </ul>
	   <?php endif; ?>
     </li>
   <?php endforeach; ?>
</ul>               
                
</div> 