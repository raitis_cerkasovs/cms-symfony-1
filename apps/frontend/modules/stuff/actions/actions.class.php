<?php
class stuffActions extends sfActions
{

  public function executeFixer2()
  {

    $fotos = FotoPeer::doSelect(new Criteria());
	
	 foreach ($fotos as $foto): 
      if ($foto->getTemp1()):
	  
	       $fileName = $foto->getPath();
	  
	       $img = new sfImage('marked/'.$fileName);
           $img->resize(263, null);
           $img->saveAs('fixer_thumbs/'.$fileName);

	   endif;
     endforeach; 
  }

  public function executeFixer()
  {

    $fotos = FotoPeer::doSelect(new Criteria());
	
	 foreach ($fotos as $foto): 
      if ($foto->getIdSubgaleries1() == $foto->getIdGaleries())
	     $foto->setIdGaleries(NULL);
		 $foto->save();
	  echo $foto->getPath();

    endforeach; 
  }
     
  public function executeAdminWelcome()
  {
    $this->auth = "
        You are registered as a page Administrator. The administration panel is displayed at the top of page content.<br><br>
		To edit pages click on desired page link within the main menu.";
  }

  public function executeLogin()
  {
 
  }
  
   public function execute404()
  {
 
  }
   public function execute500()
  {
 
  }
  
}
