<?php
// auto-generated by sfPropelCrud
// date: 2008/11/02 13:03:16
?>
<?php

/**
 * subcategories1 actions.
 *
 * @package    letapolise
 * @subpackage subcategories1
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 3335 2007-01-23 16:19:56Z fabien $
 */
class subcategories1Actions extends sfActions
{

public function handleErrorUpdate()
{
return $this->forward('subcategories1','create');

}
  public function executeIndex()
  {
    return $this->forward('subcategories1', 'list');
  }

  public function executeList()
  {
    $this->subcategories1s = Subcategories1Peer::doSelect(new Criteria());
  }

  public function executeShow()
  {
    $this->subcategories1 = Subcategories1Peer::retrieveByPk($this->getRequestParameter('id'));
    $this->forward404Unless($this->subcategories1);
  }

  public function executeCreate()
  {
	$this->help = "
        Create sub-menu.";  
		
    $this->subcategories1 = new Subcategories1();

    $this->setTemplate('edit');
  }

  public function executeEdit()
  {
	$this->help = "
        Edit sub-menu.";  
	
    $this->subcategories1 = Subcategories1Peer::retrieveByPk($this->getRequestParameter('id'));
    $this->forward404Unless($this->subcategories1);
  }

  public function executeUpdate()
  {
    if (!$this->getRequestParameter('id'))
    {  $name = $this->getRequestParameter('name');
	   $articles = new Articles();
       $articles->setBody($name);
	   $articles->save();
	   $temp_art = $articles->getId();
	   $subcategories1 = new Subcategories1();  
	   
    $subcategories1->setId($this->getRequestParameter('id'));
    $subcategories1->setIdCategories($this->getRequestParameter('id_categories') ? $this->getRequestParameter('id_categories') : null);
    $subcategories1->setIdArticles($temp_art);
    $subcategories1->setName($this->getRequestParameter('name'));
    // $subcategories1->setExtmodule($this->getRequestParameter('extmodule'));
    // $subcategories1->setParentId($this->getRequestParameter('parent_id'));
    // $subcategories1->setPosition($this->getRequestParameter('position'));
    $subcategories1->setLangId($this->getUser()->getCulture());
    // $subcategories1->setStatus($this->getRequestParameter('status'));
    // $subcategories1->setIsvert($this->getRequestParameter('isvert'));

    $subcategories1->save();

    return $this->redirect('categories/list');
	
    }
    else
    {
      $subcategories1 = Subcategories1Peer::retrieveByPk($this->getRequestParameter('id'));
      $this->forward404Unless($subcategories1);   

    //$subcategories1->setId($this->getRequestParameter('id'));
    $subcategories1->setIdCategories($this->getRequestParameter('id_categories') ? $this->getRequestParameter('id_categories') : null);
    //$subcategories1->setIdArticles($this->getRequestParameter('id_articles') ? $this->getRequestParameter('id_articles') : null);
    $subcategories1->setName($this->getRequestParameter('name'));
    //$subcategories1->setExtmodule($this->getRequestParameter('extmodule'));
    //$subcategories1->setParentId($this->getRequestParameter('parent_id'));
    //$subcategories1->setPosition($this->getRequestParameter('position'));
    $subcategories1->setLangId($this->getUser()->getCulture());
    //$subcategories1->setStatus($this->getRequestParameter('status'));
    //$subcategories1->setIsvert($this->getRequestParameter('isvert'));

    $subcategories1->save();

    return $this->redirect('categories/list');
	}
}
	
	
  public function executeDelete()
  {
    $subcategories1 = Subcategories1Peer::retrieveByPk($this->getRequestParameter('id'));

    $this->forward404Unless($subcategories1);

    $subcategories1->delete();

    return $this->redirect('categories/list');
  }
}
