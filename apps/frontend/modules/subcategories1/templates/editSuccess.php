﻿<div style="padding-left:10px;">
<?php use_helper('Object') ?>
<?php use_helper('Validation') ?>

<?php echo form_tag('subcategories1/update') ?>
<?php echo object_input_hidden_tag($subcategories1, 'getId') ?>

<?php echo object_select_tag($subcategories1, 'getIdCategories', array ('related_class' => 'Categories')) ?><br><br>Name:<br>
 <?php echo form_error('name')?>
<?php echo object_input_tag($subcategories1, 'getName', array ('size' => 15));?><br><br>
<?php echo submit_tag('OK') ?>

<?php if ($subcategories1->getId()): ?>
  &nbsp;<?php echo link_to('Delete', 'subcategories1/delete?id='.$subcategories1->getId(), 'post=true&confirm=Are you sure?') ?>
  &nbsp;<?php echo link_to('Cancel', 'categories/list') ?>
<?php else: ?>
  &nbsp;<?php echo link_to('Cancel', 'categories/list') ?>
<?php endif; ?>
</form>
</div>