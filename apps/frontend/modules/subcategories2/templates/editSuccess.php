﻿<div style="padding-left:10px;">
<?php use_helper('Object') ?>
<?php use_helper('Validation') ?>

<?php echo form_tag('subcategories2/update') ?>
<?php echo object_input_hidden_tag($subcategories2, 'getId') ?>

<?php echo object_select_tag($subcategories2, 'getIdSubcategories1', array ('related_class' => 'Subcategories1')) ?><br><br>Name:<br>
 <?php echo form_error('name')?>
<?php echo object_input_tag($subcategories2, 'getName', array ('size' => 15));?><br><br>
<?php echo submit_tag('OK') ?>

<?php if ($subcategories2->getId()): ?>
  &nbsp;<?php echo link_to('Delete', 'subcategories2/delete?id='.$subcategories2->getId(), 'post=true&confirm=Are you sure?') ?>
  &nbsp;<?php echo link_to('Cancel', 'categories/list') ?>
<?php else: ?>
  &nbsp;<?php echo link_to('Cancel', 'categories/list') ?>
<?php endif; ?>
</form>
</div>