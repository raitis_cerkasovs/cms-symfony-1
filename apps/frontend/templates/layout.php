﻿<?php use_helper('I18N') ?>
<?php use_helper('Javascript');?>
<?php use_helper('Validation');?>

<?php if ($sf_user->isAuthenticated()) $r_flag = true; else $r_flag = false; ?>

<?php if ($sf_user->getCulture() == 'lv_LV'): $lang = 'lv'; $langNr =1; endif; ?>
<?php if ($sf_user->getCulture() == 'ru_RU'): $lang = 'ru'; $langNr =2; endif; ?>
<?php if ($sf_user->getCulture() == 'en_GB'): $lang = 'en'; $langNr =3; endif; ?>

<!DOCTYPE html>
<html>

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
   
    <title>CMS on Symfony 1.0</title>   
    
    <link rel="icon" href="favicon.png" type="image/x-icon" />

    <script type="text/javascript" src="/js/jquery-full.js"></script>
    <script type="text/javascript">
      $j=jQuery.noConflict();
    </script>

    <link rel="stylesheet" type="text/css" href="/css/style.css" />
    
</head>

<body>
  
<div id="container">

 <div id="helper"> 
     <?php if (isset($auth)) echo $auth; ?>
     <?php if (isset($help)) echo $help; ?>
 </div>


	<div id="intro">
		<div id="pageHeader">
                        
			<h1><span style="position:absolute; right: 30px; top: 25px;">Raitis Cerkasovs</span></h1>
			<h2><span style="position:absolute; right: 30px; top: 64px;">CMS driven by Symfony 1</span></h2>
			<h4><span style="position:absolute; right: 30px; top: 5px;">
                <?php echo link_to('EN', 'langs/SetEn');?>&nbsp;|&nbsp;
                <?php echo link_to('LV', 'langs/SetLv');?>
            </span></h4>

		</div>
        
		<div id="preamble">
        
          <?php if ($r_flag): ?>
            <div>
              <?php include_component_slot('admin') ?>
            </div>
          <?php endif;?>
            
          <?php echo $sf_data->getRaw('sf_content') ?>
            
		</div>
        
        <div id="footer">
        
            Copyright

        </div>
        
	</div>



	<div id="linkList">
		<div id="linkList2">
        			                
            <?php  include_component_slot('sidebar') ?>             
            
		</div>
	</div>


</div>
</body>

</html>