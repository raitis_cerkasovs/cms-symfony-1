
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

#-----------------------------------------------------------------------------
#-- messages
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `messages`;


CREATE TABLE `messages`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`email` VARCHAR(255),
	`body` TEXT,
	`host` VARCHAR(25),
	`temp` VARCHAR(225),
	`publish_date` DATETIME,
	PRIMARY KEY (`id`)
)Type=InnoDB;

#-----------------------------------------------------------------------------
#-- subcategories2
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `subcategories2`;


CREATE TABLE `subcategories2`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`id_subcategories1` INTEGER  NOT NULL,
	`id_articles` INTEGER,
	`name` VARCHAR(64)  NOT NULL,
	`extModule` VARCHAR(254),
	`parent_id` INTEGER(11),
	`position` INTEGER(11),
	`lang_id` VARCHAR(11),
	`status` SMALLINT(2),
	`isVert` SMALLINT(1),
	PRIMARY KEY (`id`),
	INDEX `subcategories2_FI_1` (`id_subcategories1`),
	CONSTRAINT `subcategories2_FK_1`
		FOREIGN KEY (`id_subcategories1`)
		REFERENCES `subcategories1` (`id`),
	INDEX `subcategories2_FI_2` (`id_articles`),
	CONSTRAINT `subcategories2_FK_2`
		FOREIGN KEY (`id_articles`)
		REFERENCES `articles` (`id`)
)Type=InnoDB;

#-----------------------------------------------------------------------------
#-- bunch
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `bunch`;


CREATE TABLE `bunch`
(
	`id` SMALLINT  NOT NULL AUTO_INCREMENT,
	`Idcat` SMALLINT,
	`Idsubcat1` SMALLINT,
	`Idfoto` SMALLINT,
	`temp1` VARCHAR(45),
	`temp2` VARCHAR(45),
	`temp3` VARCHAR(45),
	PRIMARY KEY (`id`),
	INDEX `bunch_FI_1` (`Idcat`),
	CONSTRAINT `bunch_FK_1`
		FOREIGN KEY (`Idcat`)
		REFERENCES `categories` (`id`),
	INDEX `bunch_FI_2` (`Idsubcat1`),
	CONSTRAINT `bunch_FK_2`
		FOREIGN KEY (`Idsubcat1`)
		REFERENCES `subcategories1` (`id`),
	INDEX `bunch_FI_3` (`Idfoto`),
	CONSTRAINT `bunch_FK_3`
		FOREIGN KEY (`Idfoto`)
		REFERENCES `foto` (`IDfoto`)
)Type=InnoDB;

#-----------------------------------------------------------------------------
#-- foto
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `foto`;


CREATE TABLE `foto`
(
	`IDfoto` SMALLINT  NOT NULL AUTO_INCREMENT,
	`path` VARCHAR(30) default 'null' NOT NULL,
	`IDgal` SMALLINT,
	`IDslud` SMALLINT,
	`showp` VARCHAR(45),
	PRIMARY KEY (`IDfoto`),
	INDEX `foto_FI_1` (`IDgal`),
	CONSTRAINT `foto_FK_1`
		FOREIGN KEY (`IDgal`)
		REFERENCES `categories` (`id`),
	INDEX `foto_FI_2` (`IDslud`),
	CONSTRAINT `foto_FK_2`
		FOREIGN KEY (`IDslud`)
		REFERENCES `subcategories1` (`id`)
)Type=InnoDB;

#-----------------------------------------------------------------------------
#-- categories
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `categories`;


CREATE TABLE `categories`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`id_articles` INTEGER,
	`name` VARCHAR(64)  NOT NULL,
	`extModule` VARCHAR(254),
	`parent_id` INTEGER(11),
	`position` INTEGER(11),
	`lang_id` VARCHAR(11),
	`status` VARCHAR(45),
	`isVert` VARCHAR(45),
	PRIMARY KEY (`id`),
	INDEX `categories_FI_1` (`id_articles`),
	CONSTRAINT `categories_FK_1`
		FOREIGN KEY (`id_articles`)
		REFERENCES `articles` (`id`)
)Type=InnoDB;

#-----------------------------------------------------------------------------
#-- subcategories1
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `subcategories1`;


CREATE TABLE `subcategories1`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`id_categories` INTEGER  NOT NULL,
	`id_articles` INTEGER,
	`name` VARCHAR(64)  NOT NULL,
	`extModule` VARCHAR(254),
	`parent_id` INTEGER(11),
	`position` INTEGER(11),
	`lang_id` VARCHAR(11),
	`status` SMALLINT(2),
	`isVert` SMALLINT(1),
	PRIMARY KEY (`id`),
	INDEX `subcategories1_FI_1` (`id_categories`),
	CONSTRAINT `subcategories1_FK_1`
		FOREIGN KEY (`id_categories`)
		REFERENCES `categories` (`id`),
	INDEX `subcategories1_FI_2` (`id_articles`),
	CONSTRAINT `subcategories1_FK_2`
		FOREIGN KEY (`id_articles`)
		REFERENCES `articles` (`id`)
)Type=InnoDB;

#-----------------------------------------------------------------------------
#-- articles
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `articles`;


CREATE TABLE `articles`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`temp` VARCHAR(8),
	`title` VARCHAR(225),
	`body` TEXT(254),
	`active` INTEGER(1),
	`thumbnail` VARCHAR(225),
	`publish_date` DATETIME,
	PRIMARY KEY (`id`)
)Type=InnoDB;

#-----------------------------------------------------------------------------
#-- slot
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `slot`;


CREATE TABLE `slot`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(8),
	`lang` VARCHAR(8),
	`title` VARCHAR(225),
	`body` TEXT(254),
	`active` INTEGER(1),
	`thumbnail` VARCHAR(225),
	`publish_date` DATETIME,
	PRIMARY KEY (`id`)
)Type=InnoDB;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
