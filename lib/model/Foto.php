<?php

/**
 * Subclass for representing a row from the 'foto' table.
 *
 * 
 *
 * @package lib.model
 */ 
class Foto extends BaseFoto
{
public function getFotoBack() {
return image_tag('/'.sfConfig::get('sf_upload_dir_name').'/thumbnail/'.$this->getPath());
}

public function __toString() {

return $this->getPath();}

}
