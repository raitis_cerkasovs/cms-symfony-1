<?php

/**
 * Subclass for representing a row from the 'galeries' table.
 *
 * 
 *
 * @package lib.model
 */ 
 
class Galeries extends BaseGaleries
{

public function __toString()
 {
  return $this->getName();
 }

}
