<?php

/**
 * Subclass for representing a row from the 'subcategories1' table.
 *
 * 
 *
 * @package lib.model
 */ 
class Subcategories1 extends BaseSubcategories1
{

public function __toString() {

return $this->getName();}

}
