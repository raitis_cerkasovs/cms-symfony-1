<?php

/**
 * Subclass for representing a row from the 'subcategories2' table.
 *
 * 
 *
 * @package lib.model
 */ 
class Subcategories2 extends BaseSubcategories2
{

public function __toString() {

return $this->getName();}

}
