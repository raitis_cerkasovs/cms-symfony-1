<?php

/**
 * Subclass for representing a row from the 'subgaleries1' table.
 *
 * 
 *
 * @package lib.model
 */ 
class Subgaleries1 extends BaseSubgaleries1
{

public function __toString()
 {
  return $this->getName();
 }

}
