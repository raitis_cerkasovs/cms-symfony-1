<?php



class ArticlesoutMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.ArticlesoutMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('articlesout');
		$tMap->setPhpName('Articlesout');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('TEMP', 'Temp', 'string', CreoleTypes::VARCHAR, false, 8);

		$tMap->addColumn('TITLE', 'Title', 'string', CreoleTypes::VARCHAR, false, 225);

		$tMap->addColumn('BODY', 'Body', 'string', CreoleTypes::LONGVARCHAR, false, 254);

		$tMap->addColumn('ACTIVE', 'Active', 'int', CreoleTypes::INTEGER, false, 1);

		$tMap->addColumn('THUMBNAIL', 'Thumbnail', 'string', CreoleTypes::VARCHAR, false, 225);

		$tMap->addColumn('PUBLISH_DATE', 'PublishDate', 'int', CreoleTypes::TIMESTAMP, false, null);

	} 
} 