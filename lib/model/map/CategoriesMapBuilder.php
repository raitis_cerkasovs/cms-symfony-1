<?php



class CategoriesMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.CategoriesMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('categories');
		$tMap->setPhpName('Categories');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addForeignKey('ID_ARTICLES', 'IdArticles', 'int', CreoleTypes::INTEGER, 'articles', 'ID', false, null);

		$tMap->addColumn('NAME', 'Name', 'string', CreoleTypes::VARCHAR, true, 64);

		$tMap->addColumn('EXTMODULE', 'Extmodule', 'string', CreoleTypes::VARCHAR, false, 254);

		$tMap->addColumn('PARENT_ID', 'ParentId', 'int', CreoleTypes::INTEGER, false, 11);

		$tMap->addColumn('POSITION', 'Position', 'int', CreoleTypes::INTEGER, false, 11);

		$tMap->addColumn('LANG_ID', 'LangId', 'string', CreoleTypes::VARCHAR, false, 11);

		$tMap->addColumn('STATUS', 'Status', 'string', CreoleTypes::VARCHAR, false, 45);

		$tMap->addColumn('ISVERT', 'Isvert', 'string', CreoleTypes::VARCHAR, false, 45);

	} 
} 