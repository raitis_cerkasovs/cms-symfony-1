<?php



class FotoMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.FotoMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('foto');
		$tMap->setPhpName('Foto');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::SMALLINT, true, null);

		$tMap->addColumn('PATH', 'Path', 'string', CreoleTypes::VARCHAR, false, 60);

		$tMap->addColumn('CREATED', 'Created', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addForeignKey('ID_SPECIES', 'IdSpecies', 'int', CreoleTypes::SMALLINT, 'species', 'ID', false, null);

		$tMap->addForeignKey('ID_SUBGALERIES1', 'IdSubgaleries1', 'int', CreoleTypes::SMALLINT, 'subgaleries1', 'ID', false, null);

		$tMap->addForeignKey('ID_GALERIES', 'IdGaleries', 'int', CreoleTypes::SMALLINT, 'galeries', 'ID', false, null);

		$tMap->addColumn('IS_HORIZONTAL', 'IsHorizontal', 'string', CreoleTypes::VARCHAR, false, 1);

		$tMap->addColumn('TEXT', 'Text', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('TEMP1', 'Temp1', 'string', CreoleTypes::VARCHAR, false, 225);

		$tMap->addColumn('TEMP2', 'Temp2', 'string', CreoleTypes::VARCHAR, false, 225);

		$tMap->addColumn('TEMP3', 'Temp3', 'string', CreoleTypes::VARCHAR, false, 225);

		$tMap->addColumn('TEMP4', 'Temp4', 'string', CreoleTypes::LONGVARCHAR, false, null);

	} 
} 