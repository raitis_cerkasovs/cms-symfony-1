<?php



class Subcategories2MapBuilder {

	
	const CLASS_NAME = 'lib.model.map.Subcategories2MapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('subcategories2');
		$tMap->setPhpName('Subcategories2');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addForeignKey('ID_SUBCATEGORIES1', 'IdSubcategories1', 'int', CreoleTypes::INTEGER, 'subcategories1', 'ID', true, null);

		$tMap->addForeignKey('ID_ARTICLES', 'IdArticles', 'int', CreoleTypes::INTEGER, 'articles', 'ID', false, null);

		$tMap->addColumn('NAME', 'Name', 'string', CreoleTypes::VARCHAR, true, 64);

		$tMap->addColumn('EXTMODULE', 'Extmodule', 'string', CreoleTypes::VARCHAR, false, 254);

		$tMap->addColumn('PARENT_ID', 'ParentId', 'int', CreoleTypes::INTEGER, false, 11);

		$tMap->addColumn('POSITION', 'Position', 'int', CreoleTypes::INTEGER, false, 11);

		$tMap->addColumn('LANG_ID', 'LangId', 'string', CreoleTypes::VARCHAR, false, 11);

		$tMap->addColumn('STATUS', 'Status', 'int', CreoleTypes::SMALLINT, false, 2);

		$tMap->addColumn('ISVERT', 'Isvert', 'int', CreoleTypes::SMALLINT, false, 1);

	} 
} 