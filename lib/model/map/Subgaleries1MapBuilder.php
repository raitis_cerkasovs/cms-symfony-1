<?php



class Subgaleries1MapBuilder {

	
	const CLASS_NAME = 'lib.model.map.Subgaleries1MapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('subgaleries1');
		$tMap->setPhpName('Subgaleries1');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::SMALLINT, true, null);

		$tMap->addForeignKey('ID_GALERIES', 'IdGaleries', 'int', CreoleTypes::SMALLINT, 'galeries', 'ID', false, null);

		$tMap->addColumn('NAME', 'Name', 'string', CreoleTypes::VARCHAR, false, 254);

		$tMap->addColumn('POSITION', 'Position', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('CREATED', 'Created', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('TEMP1', 'Temp1', 'string', CreoleTypes::VARCHAR, false, 225);

		$tMap->addColumn('TEMP2', 'Temp2', 'string', CreoleTypes::VARCHAR, false, 225);

		$tMap->addColumn('TEMP3', 'Temp3', 'string', CreoleTypes::VARCHAR, false, 225);

		$tMap->addColumn('TEMP4', 'Temp4', 'string', CreoleTypes::LONGVARCHAR, false, null);

	} 
} 