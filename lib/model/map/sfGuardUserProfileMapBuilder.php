<?php



class sfGuardUserProfileMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.sfGuardUserProfileMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('sf_guard_user_profile');
		$tMap->setPhpName('sfGuardUserProfile');

		$tMap->setUseIdGenerator(true);

		$tMap->addForeignKey('USER_ID', 'UserId', 'int', CreoleTypes::INTEGER, 'sf_guard_user', 'ID', true, null);

		$tMap->addColumn('EMAIL', 'Email', 'string', CreoleTypes::VARCHAR, false, 80);

		$tMap->addColumn('FULLNAME', 'Fullname', 'string', CreoleTypes::VARCHAR, false, 80);

		$tMap->addColumn('VALIDATE', 'Validate', 'string', CreoleTypes::VARCHAR, false, 17);

		$tMap->addColumn('TEMP1', 'Temp1', 'string', CreoleTypes::VARCHAR, false, 80);

		$tMap->addColumn('TEMP2', 'Temp2', 'string', CreoleTypes::VARCHAR, false, 80);

		$tMap->addColumn('TEMP3', 'Temp3', 'string', CreoleTypes::VARCHAR, false, 17);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

	} 
} 