<?php


abstract class BaseArticles extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $temp;


	
	protected $title;


	
	protected $body;


	
	protected $active;


	
	protected $thumbnail;


	
	protected $publish_date;

	
	protected $collSubcategories2s;

	
	protected $lastSubcategories2Criteria = null;

	
	protected $collCategoriess;

	
	protected $lastCategoriesCriteria = null;

	
	protected $collSubcategories1s;

	
	protected $lastSubcategories1Criteria = null;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getTemp()
	{

		return $this->temp;
	}

	
	public function getTitle()
	{

		return $this->title;
	}

	
	public function getBody()
	{

		return $this->body;
	}

	
	public function getActive()
	{

		return $this->active;
	}

	
	public function getThumbnail()
	{

		return $this->thumbnail;
	}

	
	public function getPublishDate($format = 'Y-m-d H:i:s')
	{

		if ($this->publish_date === null || $this->publish_date === '') {
			return null;
		} elseif (!is_int($this->publish_date)) {
						$ts = strtotime($this->publish_date);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [publish_date] as date/time value: " . var_export($this->publish_date, true));
			}
		} else {
			$ts = $this->publish_date;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function setId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = ArticlesPeer::ID;
		}

	} 
	
	public function setTemp($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->temp !== $v) {
			$this->temp = $v;
			$this->modifiedColumns[] = ArticlesPeer::TEMP;
		}

	} 
	
	public function setTitle($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->title !== $v) {
			$this->title = $v;
			$this->modifiedColumns[] = ArticlesPeer::TITLE;
		}

	} 
	
	public function setBody($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->body !== $v) {
			$this->body = $v;
			$this->modifiedColumns[] = ArticlesPeer::BODY;
		}

	} 
	
	public function setActive($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->active !== $v) {
			$this->active = $v;
			$this->modifiedColumns[] = ArticlesPeer::ACTIVE;
		}

	} 
	
	public function setThumbnail($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->thumbnail !== $v) {
			$this->thumbnail = $v;
			$this->modifiedColumns[] = ArticlesPeer::THUMBNAIL;
		}

	} 
	
	public function setPublishDate($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [publish_date] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->publish_date !== $ts) {
			$this->publish_date = $ts;
			$this->modifiedColumns[] = ArticlesPeer::PUBLISH_DATE;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->temp = $rs->getString($startcol + 1);

			$this->title = $rs->getString($startcol + 2);

			$this->body = $rs->getString($startcol + 3);

			$this->active = $rs->getInt($startcol + 4);

			$this->thumbnail = $rs->getString($startcol + 5);

			$this->publish_date = $rs->getTimestamp($startcol + 6, null);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 7; 
		} catch (Exception $e) {
			throw new PropelException("Error populating Articles object", $e);
		}
	}

	
	public function delete($con = null)
	{

    foreach (sfMixer::getCallables('BaseArticles:delete:pre') as $callable)
    {
      $ret = call_user_func($callable, $this, $con);
      if ($ret)
      {
        return;
      }
    }


		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ArticlesPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			ArticlesPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	

    foreach (sfMixer::getCallables('BaseArticles:delete:post') as $callable)
    {
      call_user_func($callable, $this, $con);
    }

  }
	
	public function save($con = null)
	{

    foreach (sfMixer::getCallables('BaseArticles:save:pre') as $callable)
    {
      $affectedRows = call_user_func($callable, $this, $con);
      if (is_int($affectedRows))
      {
        return $affectedRows;
      }
    }


		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ArticlesPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
    foreach (sfMixer::getCallables('BaseArticles:save:post') as $callable)
    {
      call_user_func($callable, $this, $con, $affectedRows);
    }

			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = ArticlesPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += ArticlesPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			if ($this->collSubcategories2s !== null) {
				foreach($this->collSubcategories2s as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collCategoriess !== null) {
				foreach($this->collCategoriess as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collSubcategories1s !== null) {
				foreach($this->collSubcategories1s as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = ArticlesPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collSubcategories2s !== null) {
					foreach($this->collSubcategories2s as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collCategoriess !== null) {
					foreach($this->collCategoriess as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collSubcategories1s !== null) {
					foreach($this->collSubcategories1s as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ArticlesPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getTemp();
				break;
			case 2:
				return $this->getTitle();
				break;
			case 3:
				return $this->getBody();
				break;
			case 4:
				return $this->getActive();
				break;
			case 5:
				return $this->getThumbnail();
				break;
			case 6:
				return $this->getPublishDate();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ArticlesPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getTemp(),
			$keys[2] => $this->getTitle(),
			$keys[3] => $this->getBody(),
			$keys[4] => $this->getActive(),
			$keys[5] => $this->getThumbnail(),
			$keys[6] => $this->getPublishDate(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ArticlesPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setTemp($value);
				break;
			case 2:
				$this->setTitle($value);
				break;
			case 3:
				$this->setBody($value);
				break;
			case 4:
				$this->setActive($value);
				break;
			case 5:
				$this->setThumbnail($value);
				break;
			case 6:
				$this->setPublishDate($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ArticlesPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setTemp($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setTitle($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setBody($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setActive($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setThumbnail($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setPublishDate($arr[$keys[6]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(ArticlesPeer::DATABASE_NAME);

		if ($this->isColumnModified(ArticlesPeer::ID)) $criteria->add(ArticlesPeer::ID, $this->id);
		if ($this->isColumnModified(ArticlesPeer::TEMP)) $criteria->add(ArticlesPeer::TEMP, $this->temp);
		if ($this->isColumnModified(ArticlesPeer::TITLE)) $criteria->add(ArticlesPeer::TITLE, $this->title);
		if ($this->isColumnModified(ArticlesPeer::BODY)) $criteria->add(ArticlesPeer::BODY, $this->body);
		if ($this->isColumnModified(ArticlesPeer::ACTIVE)) $criteria->add(ArticlesPeer::ACTIVE, $this->active);
		if ($this->isColumnModified(ArticlesPeer::THUMBNAIL)) $criteria->add(ArticlesPeer::THUMBNAIL, $this->thumbnail);
		if ($this->isColumnModified(ArticlesPeer::PUBLISH_DATE)) $criteria->add(ArticlesPeer::PUBLISH_DATE, $this->publish_date);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(ArticlesPeer::DATABASE_NAME);

		$criteria->add(ArticlesPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setTemp($this->temp);

		$copyObj->setTitle($this->title);

		$copyObj->setBody($this->body);

		$copyObj->setActive($this->active);

		$copyObj->setThumbnail($this->thumbnail);

		$copyObj->setPublishDate($this->publish_date);


		if ($deepCopy) {
									$copyObj->setNew(false);

			foreach($this->getSubcategories2s() as $relObj) {
				$copyObj->addSubcategories2($relObj->copy($deepCopy));
			}

			foreach($this->getCategoriess() as $relObj) {
				$copyObj->addCategories($relObj->copy($deepCopy));
			}

			foreach($this->getSubcategories1s() as $relObj) {
				$copyObj->addSubcategories1($relObj->copy($deepCopy));
			}

		} 

		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new ArticlesPeer();
		}
		return self::$peer;
	}

	
	public function initSubcategories2s()
	{
		if ($this->collSubcategories2s === null) {
			$this->collSubcategories2s = array();
		}
	}

	
	public function getSubcategories2s($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseSubcategories2Peer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collSubcategories2s === null) {
			if ($this->isNew()) {
			   $this->collSubcategories2s = array();
			} else {

				$criteria->add(Subcategories2Peer::ID_ARTICLES, $this->getId());

				Subcategories2Peer::addSelectColumns($criteria);
				$this->collSubcategories2s = Subcategories2Peer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(Subcategories2Peer::ID_ARTICLES, $this->getId());

				Subcategories2Peer::addSelectColumns($criteria);
				if (!isset($this->lastSubcategories2Criteria) || !$this->lastSubcategories2Criteria->equals($criteria)) {
					$this->collSubcategories2s = Subcategories2Peer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastSubcategories2Criteria = $criteria;
		return $this->collSubcategories2s;
	}

	
	public function countSubcategories2s($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseSubcategories2Peer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(Subcategories2Peer::ID_ARTICLES, $this->getId());

		return Subcategories2Peer::doCount($criteria, $distinct, $con);
	}

	
	public function addSubcategories2(Subcategories2 $l)
	{
		$this->collSubcategories2s[] = $l;
		$l->setArticles($this);
	}


	
	public function getSubcategories2sJoinSubcategories1($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseSubcategories2Peer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collSubcategories2s === null) {
			if ($this->isNew()) {
				$this->collSubcategories2s = array();
			} else {

				$criteria->add(Subcategories2Peer::ID_ARTICLES, $this->getId());

				$this->collSubcategories2s = Subcategories2Peer::doSelectJoinSubcategories1($criteria, $con);
			}
		} else {
									
			$criteria->add(Subcategories2Peer::ID_ARTICLES, $this->getId());

			if (!isset($this->lastSubcategories2Criteria) || !$this->lastSubcategories2Criteria->equals($criteria)) {
				$this->collSubcategories2s = Subcategories2Peer::doSelectJoinSubcategories1($criteria, $con);
			}
		}
		$this->lastSubcategories2Criteria = $criteria;

		return $this->collSubcategories2s;
	}

	
	public function initCategoriess()
	{
		if ($this->collCategoriess === null) {
			$this->collCategoriess = array();
		}
	}

	
	public function getCategoriess($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseCategoriesPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collCategoriess === null) {
			if ($this->isNew()) {
			   $this->collCategoriess = array();
			} else {

				$criteria->add(CategoriesPeer::ID_ARTICLES, $this->getId());

				CategoriesPeer::addSelectColumns($criteria);
				$this->collCategoriess = CategoriesPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(CategoriesPeer::ID_ARTICLES, $this->getId());

				CategoriesPeer::addSelectColumns($criteria);
				if (!isset($this->lastCategoriesCriteria) || !$this->lastCategoriesCriteria->equals($criteria)) {
					$this->collCategoriess = CategoriesPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastCategoriesCriteria = $criteria;
		return $this->collCategoriess;
	}

	
	public function countCategoriess($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseCategoriesPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(CategoriesPeer::ID_ARTICLES, $this->getId());

		return CategoriesPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addCategories(Categories $l)
	{
		$this->collCategoriess[] = $l;
		$l->setArticles($this);
	}

	
	public function initSubcategories1s()
	{
		if ($this->collSubcategories1s === null) {
			$this->collSubcategories1s = array();
		}
	}

	
	public function getSubcategories1s($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseSubcategories1Peer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collSubcategories1s === null) {
			if ($this->isNew()) {
			   $this->collSubcategories1s = array();
			} else {

				$criteria->add(Subcategories1Peer::ID_ARTICLES, $this->getId());

				Subcategories1Peer::addSelectColumns($criteria);
				$this->collSubcategories1s = Subcategories1Peer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(Subcategories1Peer::ID_ARTICLES, $this->getId());

				Subcategories1Peer::addSelectColumns($criteria);
				if (!isset($this->lastSubcategories1Criteria) || !$this->lastSubcategories1Criteria->equals($criteria)) {
					$this->collSubcategories1s = Subcategories1Peer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastSubcategories1Criteria = $criteria;
		return $this->collSubcategories1s;
	}

	
	public function countSubcategories1s($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseSubcategories1Peer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(Subcategories1Peer::ID_ARTICLES, $this->getId());

		return Subcategories1Peer::doCount($criteria, $distinct, $con);
	}

	
	public function addSubcategories1(Subcategories1 $l)
	{
		$this->collSubcategories1s[] = $l;
		$l->setArticles($this);
	}


	
	public function getSubcategories1sJoinCategories($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseSubcategories1Peer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collSubcategories1s === null) {
			if ($this->isNew()) {
				$this->collSubcategories1s = array();
			} else {

				$criteria->add(Subcategories1Peer::ID_ARTICLES, $this->getId());

				$this->collSubcategories1s = Subcategories1Peer::doSelectJoinCategories($criteria, $con);
			}
		} else {
									
			$criteria->add(Subcategories1Peer::ID_ARTICLES, $this->getId());

			if (!isset($this->lastSubcategories1Criteria) || !$this->lastSubcategories1Criteria->equals($criteria)) {
				$this->collSubcategories1s = Subcategories1Peer::doSelectJoinCategories($criteria, $con);
			}
		}
		$this->lastSubcategories1Criteria = $criteria;

		return $this->collSubcategories1s;
	}


  public function __call($method, $arguments)
  {
    if (!$callable = sfMixer::getCallable('BaseArticles:'.$method))
    {
      throw new sfException(sprintf('Call to undefined method BaseArticles::%s', $method));
    }

    array_unshift($arguments, $this);

    return call_user_func_array($callable, $arguments);
  }


} 