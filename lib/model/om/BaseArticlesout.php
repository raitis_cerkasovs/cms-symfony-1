<?php


abstract class BaseArticlesout extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $temp;


	
	protected $title;


	
	protected $body;


	
	protected $active;


	
	protected $thumbnail;


	
	protected $publish_date;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getTemp()
	{

		return $this->temp;
	}

	
	public function getTitle()
	{

		return $this->title;
	}

	
	public function getBody()
	{

		return $this->body;
	}

	
	public function getActive()
	{

		return $this->active;
	}

	
	public function getThumbnail()
	{

		return $this->thumbnail;
	}

	
	public function getPublishDate($format = 'Y-m-d H:i:s')
	{

		if ($this->publish_date === null || $this->publish_date === '') {
			return null;
		} elseif (!is_int($this->publish_date)) {
						$ts = strtotime($this->publish_date);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [publish_date] as date/time value: " . var_export($this->publish_date, true));
			}
		} else {
			$ts = $this->publish_date;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function setId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = ArticlesoutPeer::ID;
		}

	} 
	
	public function setTemp($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->temp !== $v) {
			$this->temp = $v;
			$this->modifiedColumns[] = ArticlesoutPeer::TEMP;
		}

	} 
	
	public function setTitle($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->title !== $v) {
			$this->title = $v;
			$this->modifiedColumns[] = ArticlesoutPeer::TITLE;
		}

	} 
	
	public function setBody($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->body !== $v) {
			$this->body = $v;
			$this->modifiedColumns[] = ArticlesoutPeer::BODY;
		}

	} 
	
	public function setActive($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->active !== $v) {
			$this->active = $v;
			$this->modifiedColumns[] = ArticlesoutPeer::ACTIVE;
		}

	} 
	
	public function setThumbnail($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->thumbnail !== $v) {
			$this->thumbnail = $v;
			$this->modifiedColumns[] = ArticlesoutPeer::THUMBNAIL;
		}

	} 
	
	public function setPublishDate($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [publish_date] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->publish_date !== $ts) {
			$this->publish_date = $ts;
			$this->modifiedColumns[] = ArticlesoutPeer::PUBLISH_DATE;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->temp = $rs->getString($startcol + 1);

			$this->title = $rs->getString($startcol + 2);

			$this->body = $rs->getString($startcol + 3);

			$this->active = $rs->getInt($startcol + 4);

			$this->thumbnail = $rs->getString($startcol + 5);

			$this->publish_date = $rs->getTimestamp($startcol + 6, null);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 7; 
		} catch (Exception $e) {
			throw new PropelException("Error populating Articlesout object", $e);
		}
	}

	
	public function delete($con = null)
	{

    foreach (sfMixer::getCallables('BaseArticlesout:delete:pre') as $callable)
    {
      $ret = call_user_func($callable, $this, $con);
      if ($ret)
      {
        return;
      }
    }


		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ArticlesoutPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			ArticlesoutPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	

    foreach (sfMixer::getCallables('BaseArticlesout:delete:post') as $callable)
    {
      call_user_func($callable, $this, $con);
    }

  }
	
	public function save($con = null)
	{

    foreach (sfMixer::getCallables('BaseArticlesout:save:pre') as $callable)
    {
      $affectedRows = call_user_func($callable, $this, $con);
      if (is_int($affectedRows))
      {
        return $affectedRows;
      }
    }


		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ArticlesoutPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
    foreach (sfMixer::getCallables('BaseArticlesout:save:post') as $callable)
    {
      call_user_func($callable, $this, $con, $affectedRows);
    }

			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = ArticlesoutPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += ArticlesoutPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = ArticlesoutPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ArticlesoutPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getTemp();
				break;
			case 2:
				return $this->getTitle();
				break;
			case 3:
				return $this->getBody();
				break;
			case 4:
				return $this->getActive();
				break;
			case 5:
				return $this->getThumbnail();
				break;
			case 6:
				return $this->getPublishDate();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ArticlesoutPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getTemp(),
			$keys[2] => $this->getTitle(),
			$keys[3] => $this->getBody(),
			$keys[4] => $this->getActive(),
			$keys[5] => $this->getThumbnail(),
			$keys[6] => $this->getPublishDate(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ArticlesoutPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setTemp($value);
				break;
			case 2:
				$this->setTitle($value);
				break;
			case 3:
				$this->setBody($value);
				break;
			case 4:
				$this->setActive($value);
				break;
			case 5:
				$this->setThumbnail($value);
				break;
			case 6:
				$this->setPublishDate($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ArticlesoutPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setTemp($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setTitle($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setBody($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setActive($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setThumbnail($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setPublishDate($arr[$keys[6]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(ArticlesoutPeer::DATABASE_NAME);

		if ($this->isColumnModified(ArticlesoutPeer::ID)) $criteria->add(ArticlesoutPeer::ID, $this->id);
		if ($this->isColumnModified(ArticlesoutPeer::TEMP)) $criteria->add(ArticlesoutPeer::TEMP, $this->temp);
		if ($this->isColumnModified(ArticlesoutPeer::TITLE)) $criteria->add(ArticlesoutPeer::TITLE, $this->title);
		if ($this->isColumnModified(ArticlesoutPeer::BODY)) $criteria->add(ArticlesoutPeer::BODY, $this->body);
		if ($this->isColumnModified(ArticlesoutPeer::ACTIVE)) $criteria->add(ArticlesoutPeer::ACTIVE, $this->active);
		if ($this->isColumnModified(ArticlesoutPeer::THUMBNAIL)) $criteria->add(ArticlesoutPeer::THUMBNAIL, $this->thumbnail);
		if ($this->isColumnModified(ArticlesoutPeer::PUBLISH_DATE)) $criteria->add(ArticlesoutPeer::PUBLISH_DATE, $this->publish_date);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(ArticlesoutPeer::DATABASE_NAME);

		$criteria->add(ArticlesoutPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setTemp($this->temp);

		$copyObj->setTitle($this->title);

		$copyObj->setBody($this->body);

		$copyObj->setActive($this->active);

		$copyObj->setThumbnail($this->thumbnail);

		$copyObj->setPublishDate($this->publish_date);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new ArticlesoutPeer();
		}
		return self::$peer;
	}


  public function __call($method, $arguments)
  {
    if (!$callable = sfMixer::getCallable('BaseArticlesout:'.$method))
    {
      throw new sfException(sprintf('Call to undefined method BaseArticlesout::%s', $method));
    }

    array_unshift($arguments, $this);

    return call_user_func_array($callable, $arguments);
  }


} 