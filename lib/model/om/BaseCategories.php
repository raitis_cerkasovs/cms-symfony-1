<?php


abstract class BaseCategories extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $id_articles;


	
	protected $name;


	
	protected $extmodule;


	
	protected $parent_id;


	
	protected $position;


	
	protected $lang_id;


	
	protected $status;


	
	protected $isvert;

	
	protected $aArticles;

	
	protected $collBunchs;

	
	protected $lastBunchCriteria = null;

	
	protected $collFotos;

	
	protected $lastFotoCriteria = null;

	
	protected $collSubcategories1s;

	
	protected $lastSubcategories1Criteria = null;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getIdArticles()
	{

		return $this->id_articles;
	}

	
	public function getName()
	{

		return $this->name;
	}

	
	public function getExtmodule()
	{

		return $this->extmodule;
	}

	
	public function getParentId()
	{

		return $this->parent_id;
	}

	
	public function getPosition()
	{

		return $this->position;
	}

	
	public function getLangId()
	{

		return $this->lang_id;
	}

	
	public function getStatus()
	{

		return $this->status;
	}

	
	public function getIsvert()
	{

		return $this->isvert;
	}

	
	public function setId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = CategoriesPeer::ID;
		}

	} 
	
	public function setIdArticles($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_articles !== $v) {
			$this->id_articles = $v;
			$this->modifiedColumns[] = CategoriesPeer::ID_ARTICLES;
		}

		if ($this->aArticles !== null && $this->aArticles->getId() !== $v) {
			$this->aArticles = null;
		}

	} 
	
	public function setName($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->name !== $v) {
			$this->name = $v;
			$this->modifiedColumns[] = CategoriesPeer::NAME;
		}

	} 
	
	public function setExtmodule($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->extmodule !== $v) {
			$this->extmodule = $v;
			$this->modifiedColumns[] = CategoriesPeer::EXTMODULE;
		}

	} 
	
	public function setParentId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->parent_id !== $v) {
			$this->parent_id = $v;
			$this->modifiedColumns[] = CategoriesPeer::PARENT_ID;
		}

	} 
	
	public function setPosition($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->position !== $v) {
			$this->position = $v;
			$this->modifiedColumns[] = CategoriesPeer::POSITION;
		}

	} 
	
	public function setLangId($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->lang_id !== $v) {
			$this->lang_id = $v;
			$this->modifiedColumns[] = CategoriesPeer::LANG_ID;
		}

	} 
	
	public function setStatus($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->status !== $v) {
			$this->status = $v;
			$this->modifiedColumns[] = CategoriesPeer::STATUS;
		}

	} 
	
	public function setIsvert($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->isvert !== $v) {
			$this->isvert = $v;
			$this->modifiedColumns[] = CategoriesPeer::ISVERT;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->id_articles = $rs->getInt($startcol + 1);

			$this->name = $rs->getString($startcol + 2);

			$this->extmodule = $rs->getString($startcol + 3);

			$this->parent_id = $rs->getInt($startcol + 4);

			$this->position = $rs->getInt($startcol + 5);

			$this->lang_id = $rs->getString($startcol + 6);

			$this->status = $rs->getString($startcol + 7);

			$this->isvert = $rs->getString($startcol + 8);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 9; 
		} catch (Exception $e) {
			throw new PropelException("Error populating Categories object", $e);
		}
	}

	
	public function delete($con = null)
	{

    foreach (sfMixer::getCallables('BaseCategories:delete:pre') as $callable)
    {
      $ret = call_user_func($callable, $this, $con);
      if ($ret)
      {
        return;
      }
    }


		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(CategoriesPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			CategoriesPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	

    foreach (sfMixer::getCallables('BaseCategories:delete:post') as $callable)
    {
      call_user_func($callable, $this, $con);
    }

  }
	
	public function save($con = null)
	{

    foreach (sfMixer::getCallables('BaseCategories:save:pre') as $callable)
    {
      $affectedRows = call_user_func($callable, $this, $con);
      if (is_int($affectedRows))
      {
        return $affectedRows;
      }
    }


		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(CategoriesPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
    foreach (sfMixer::getCallables('BaseCategories:save:post') as $callable)
    {
      call_user_func($callable, $this, $con, $affectedRows);
    }

			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


												
			if ($this->aArticles !== null) {
				if ($this->aArticles->isModified()) {
					$affectedRows += $this->aArticles->save($con);
				}
				$this->setArticles($this->aArticles);
			}


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = CategoriesPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += CategoriesPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			if ($this->collBunchs !== null) {
				foreach($this->collBunchs as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collFotos !== null) {
				foreach($this->collFotos as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collSubcategories1s !== null) {
				foreach($this->collSubcategories1s as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


												
			if ($this->aArticles !== null) {
				if (!$this->aArticles->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aArticles->getValidationFailures());
				}
			}


			if (($retval = CategoriesPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collBunchs !== null) {
					foreach($this->collBunchs as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collFotos !== null) {
					foreach($this->collFotos as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collSubcategories1s !== null) {
					foreach($this->collSubcategories1s as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = CategoriesPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getIdArticles();
				break;
			case 2:
				return $this->getName();
				break;
			case 3:
				return $this->getExtmodule();
				break;
			case 4:
				return $this->getParentId();
				break;
			case 5:
				return $this->getPosition();
				break;
			case 6:
				return $this->getLangId();
				break;
			case 7:
				return $this->getStatus();
				break;
			case 8:
				return $this->getIsvert();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = CategoriesPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getIdArticles(),
			$keys[2] => $this->getName(),
			$keys[3] => $this->getExtmodule(),
			$keys[4] => $this->getParentId(),
			$keys[5] => $this->getPosition(),
			$keys[6] => $this->getLangId(),
			$keys[7] => $this->getStatus(),
			$keys[8] => $this->getIsvert(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = CategoriesPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setIdArticles($value);
				break;
			case 2:
				$this->setName($value);
				break;
			case 3:
				$this->setExtmodule($value);
				break;
			case 4:
				$this->setParentId($value);
				break;
			case 5:
				$this->setPosition($value);
				break;
			case 6:
				$this->setLangId($value);
				break;
			case 7:
				$this->setStatus($value);
				break;
			case 8:
				$this->setIsvert($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = CategoriesPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setIdArticles($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setName($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setExtmodule($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setParentId($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setPosition($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setLangId($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setStatus($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setIsvert($arr[$keys[8]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(CategoriesPeer::DATABASE_NAME);

		if ($this->isColumnModified(CategoriesPeer::ID)) $criteria->add(CategoriesPeer::ID, $this->id);
		if ($this->isColumnModified(CategoriesPeer::ID_ARTICLES)) $criteria->add(CategoriesPeer::ID_ARTICLES, $this->id_articles);
		if ($this->isColumnModified(CategoriesPeer::NAME)) $criteria->add(CategoriesPeer::NAME, $this->name);
		if ($this->isColumnModified(CategoriesPeer::EXTMODULE)) $criteria->add(CategoriesPeer::EXTMODULE, $this->extmodule);
		if ($this->isColumnModified(CategoriesPeer::PARENT_ID)) $criteria->add(CategoriesPeer::PARENT_ID, $this->parent_id);
		if ($this->isColumnModified(CategoriesPeer::POSITION)) $criteria->add(CategoriesPeer::POSITION, $this->position);
		if ($this->isColumnModified(CategoriesPeer::LANG_ID)) $criteria->add(CategoriesPeer::LANG_ID, $this->lang_id);
		if ($this->isColumnModified(CategoriesPeer::STATUS)) $criteria->add(CategoriesPeer::STATUS, $this->status);
		if ($this->isColumnModified(CategoriesPeer::ISVERT)) $criteria->add(CategoriesPeer::ISVERT, $this->isvert);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(CategoriesPeer::DATABASE_NAME);

		$criteria->add(CategoriesPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setIdArticles($this->id_articles);

		$copyObj->setName($this->name);

		$copyObj->setExtmodule($this->extmodule);

		$copyObj->setParentId($this->parent_id);

		$copyObj->setPosition($this->position);

		$copyObj->setLangId($this->lang_id);

		$copyObj->setStatus($this->status);

		$copyObj->setIsvert($this->isvert);


		if ($deepCopy) {
									$copyObj->setNew(false);

			foreach($this->getBunchs() as $relObj) {
				$copyObj->addBunch($relObj->copy($deepCopy));
			}

			foreach($this->getFotos() as $relObj) {
				$copyObj->addFoto($relObj->copy($deepCopy));
			}

			foreach($this->getSubcategories1s() as $relObj) {
				$copyObj->addSubcategories1($relObj->copy($deepCopy));
			}

		} 

		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new CategoriesPeer();
		}
		return self::$peer;
	}

	
	public function setArticles($v)
	{


		if ($v === null) {
			$this->setIdArticles(NULL);
		} else {
			$this->setIdArticles($v->getId());
		}


		$this->aArticles = $v;
	}


	
	public function getArticles($con = null)
	{
		if ($this->aArticles === null && ($this->id_articles !== null)) {
						include_once 'lib/model/om/BaseArticlesPeer.php';

			$this->aArticles = ArticlesPeer::retrieveByPK($this->id_articles, $con);

			
		}
		return $this->aArticles;
	}

	
	public function initBunchs()
	{
		if ($this->collBunchs === null) {
			$this->collBunchs = array();
		}
	}

	
	public function getBunchs($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseBunchPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collBunchs === null) {
			if ($this->isNew()) {
			   $this->collBunchs = array();
			} else {

				$criteria->add(BunchPeer::IDCAT, $this->getId());

				BunchPeer::addSelectColumns($criteria);
				$this->collBunchs = BunchPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(BunchPeer::IDCAT, $this->getId());

				BunchPeer::addSelectColumns($criteria);
				if (!isset($this->lastBunchCriteria) || !$this->lastBunchCriteria->equals($criteria)) {
					$this->collBunchs = BunchPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastBunchCriteria = $criteria;
		return $this->collBunchs;
	}

	
	public function countBunchs($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseBunchPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(BunchPeer::IDCAT, $this->getId());

		return BunchPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addBunch(Bunch $l)
	{
		$this->collBunchs[] = $l;
		$l->setCategories($this);
	}


	
	public function getBunchsJoinSubcategories1($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseBunchPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collBunchs === null) {
			if ($this->isNew()) {
				$this->collBunchs = array();
			} else {

				$criteria->add(BunchPeer::IDCAT, $this->getId());

				$this->collBunchs = BunchPeer::doSelectJoinSubcategories1($criteria, $con);
			}
		} else {
									
			$criteria->add(BunchPeer::IDCAT, $this->getId());

			if (!isset($this->lastBunchCriteria) || !$this->lastBunchCriteria->equals($criteria)) {
				$this->collBunchs = BunchPeer::doSelectJoinSubcategories1($criteria, $con);
			}
		}
		$this->lastBunchCriteria = $criteria;

		return $this->collBunchs;
	}


	
	public function getBunchsJoinFoto($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseBunchPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collBunchs === null) {
			if ($this->isNew()) {
				$this->collBunchs = array();
			} else {

				$criteria->add(BunchPeer::IDCAT, $this->getId());

				$this->collBunchs = BunchPeer::doSelectJoinFoto($criteria, $con);
			}
		} else {
									
			$criteria->add(BunchPeer::IDCAT, $this->getId());

			if (!isset($this->lastBunchCriteria) || !$this->lastBunchCriteria->equals($criteria)) {
				$this->collBunchs = BunchPeer::doSelectJoinFoto($criteria, $con);
			}
		}
		$this->lastBunchCriteria = $criteria;

		return $this->collBunchs;
	}

	
	public function initFotos()
	{
		if ($this->collFotos === null) {
			$this->collFotos = array();
		}
	}

	
	public function getFotos($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseFotoPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collFotos === null) {
			if ($this->isNew()) {
			   $this->collFotos = array();
			} else {

				$criteria->add(FotoPeer::IDGAL, $this->getId());

				FotoPeer::addSelectColumns($criteria);
				$this->collFotos = FotoPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(FotoPeer::IDGAL, $this->getId());

				FotoPeer::addSelectColumns($criteria);
				if (!isset($this->lastFotoCriteria) || !$this->lastFotoCriteria->equals($criteria)) {
					$this->collFotos = FotoPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastFotoCriteria = $criteria;
		return $this->collFotos;
	}

	
	public function countFotos($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseFotoPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(FotoPeer::IDGAL, $this->getId());

		return FotoPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addFoto(Foto $l)
	{
		$this->collFotos[] = $l;
		$l->setCategories($this);
	}


	
	public function getFotosJoinSubcategories1($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseFotoPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collFotos === null) {
			if ($this->isNew()) {
				$this->collFotos = array();
			} else {

				$criteria->add(FotoPeer::IDGAL, $this->getId());

				$this->collFotos = FotoPeer::doSelectJoinSubcategories1($criteria, $con);
			}
		} else {
									
			$criteria->add(FotoPeer::IDGAL, $this->getId());

			if (!isset($this->lastFotoCriteria) || !$this->lastFotoCriteria->equals($criteria)) {
				$this->collFotos = FotoPeer::doSelectJoinSubcategories1($criteria, $con);
			}
		}
		$this->lastFotoCriteria = $criteria;

		return $this->collFotos;
	}

	
	public function initSubcategories1s()
	{
		if ($this->collSubcategories1s === null) {
			$this->collSubcategories1s = array();
		}
	}

	
	public function getSubcategories1s($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseSubcategories1Peer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collSubcategories1s === null) {
			if ($this->isNew()) {
			   $this->collSubcategories1s = array();
			} else {

				$criteria->add(Subcategories1Peer::ID_CATEGORIES, $this->getId());

				Subcategories1Peer::addSelectColumns($criteria);
				$this->collSubcategories1s = Subcategories1Peer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(Subcategories1Peer::ID_CATEGORIES, $this->getId());

				Subcategories1Peer::addSelectColumns($criteria);
				if (!isset($this->lastSubcategories1Criteria) || !$this->lastSubcategories1Criteria->equals($criteria)) {
					$this->collSubcategories1s = Subcategories1Peer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastSubcategories1Criteria = $criteria;
		return $this->collSubcategories1s;
	}

	
	public function countSubcategories1s($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseSubcategories1Peer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(Subcategories1Peer::ID_CATEGORIES, $this->getId());

		return Subcategories1Peer::doCount($criteria, $distinct, $con);
	}

	
	public function addSubcategories1(Subcategories1 $l)
	{
		$this->collSubcategories1s[] = $l;
		$l->setCategories($this);
	}


	
	public function getSubcategories1sJoinArticles($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseSubcategories1Peer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collSubcategories1s === null) {
			if ($this->isNew()) {
				$this->collSubcategories1s = array();
			} else {

				$criteria->add(Subcategories1Peer::ID_CATEGORIES, $this->getId());

				$this->collSubcategories1s = Subcategories1Peer::doSelectJoinArticles($criteria, $con);
			}
		} else {
									
			$criteria->add(Subcategories1Peer::ID_CATEGORIES, $this->getId());

			if (!isset($this->lastSubcategories1Criteria) || !$this->lastSubcategories1Criteria->equals($criteria)) {
				$this->collSubcategories1s = Subcategories1Peer::doSelectJoinArticles($criteria, $con);
			}
		}
		$this->lastSubcategories1Criteria = $criteria;

		return $this->collSubcategories1s;
	}


  public function __call($method, $arguments)
  {
    if (!$callable = sfMixer::getCallable('BaseCategories:'.$method))
    {
      throw new sfException(sprintf('Call to undefined method BaseCategories::%s', $method));
    }

    array_unshift($arguments, $this);

    return call_user_func_array($callable, $arguments);
  }


} 