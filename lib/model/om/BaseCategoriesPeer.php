<?php


abstract class BaseCategoriesPeer {

	
	const DATABASE_NAME = 'propel';

	
	const TABLE_NAME = 'categories';

	
	const CLASS_DEFAULT = 'lib.model.Categories';

	
	const NUM_COLUMNS = 9;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID = 'categories.ID';

	
	const ID_ARTICLES = 'categories.ID_ARTICLES';

	
	const NAME = 'categories.NAME';

	
	const EXTMODULE = 'categories.EXTMODULE';

	
	const PARENT_ID = 'categories.PARENT_ID';

	
	const POSITION = 'categories.POSITION';

	
	const LANG_ID = 'categories.LANG_ID';

	
	const STATUS = 'categories.STATUS';

	
	const ISVERT = 'categories.ISVERT';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Id', 'IdArticles', 'Name', 'Extmodule', 'ParentId', 'Position', 'LangId', 'Status', 'Isvert', ),
		BasePeer::TYPE_COLNAME => array (CategoriesPeer::ID, CategoriesPeer::ID_ARTICLES, CategoriesPeer::NAME, CategoriesPeer::EXTMODULE, CategoriesPeer::PARENT_ID, CategoriesPeer::POSITION, CategoriesPeer::LANG_ID, CategoriesPeer::STATUS, CategoriesPeer::ISVERT, ),
		BasePeer::TYPE_FIELDNAME => array ('id', 'id_articles', 'name', 'extModule', 'parent_id', 'position', 'lang_id', 'status', 'isVert', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'IdArticles' => 1, 'Name' => 2, 'Extmodule' => 3, 'ParentId' => 4, 'Position' => 5, 'LangId' => 6, 'Status' => 7, 'Isvert' => 8, ),
		BasePeer::TYPE_COLNAME => array (CategoriesPeer::ID => 0, CategoriesPeer::ID_ARTICLES => 1, CategoriesPeer::NAME => 2, CategoriesPeer::EXTMODULE => 3, CategoriesPeer::PARENT_ID => 4, CategoriesPeer::POSITION => 5, CategoriesPeer::LANG_ID => 6, CategoriesPeer::STATUS => 7, CategoriesPeer::ISVERT => 8, ),
		BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'id_articles' => 1, 'name' => 2, 'extModule' => 3, 'parent_id' => 4, 'position' => 5, 'lang_id' => 6, 'status' => 7, 'isVert' => 8, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/map/CategoriesMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.map.CategoriesMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = CategoriesPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(CategoriesPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(CategoriesPeer::ID);

		$criteria->addSelectColumn(CategoriesPeer::ID_ARTICLES);

		$criteria->addSelectColumn(CategoriesPeer::NAME);

		$criteria->addSelectColumn(CategoriesPeer::EXTMODULE);

		$criteria->addSelectColumn(CategoriesPeer::PARENT_ID);

		$criteria->addSelectColumn(CategoriesPeer::POSITION);

		$criteria->addSelectColumn(CategoriesPeer::LANG_ID);

		$criteria->addSelectColumn(CategoriesPeer::STATUS);

		$criteria->addSelectColumn(CategoriesPeer::ISVERT);

	}

	const COUNT = 'COUNT(categories.ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT categories.ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(CategoriesPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(CategoriesPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = CategoriesPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = CategoriesPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return CategoriesPeer::populateObjects(CategoriesPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{

    foreach (sfMixer::getCallables('BaseCategoriesPeer:addDoSelectRS:addDoSelectRS') as $callable)
    {
      call_user_func($callable, 'BaseCategoriesPeer', $criteria, $con);
    }


		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			CategoriesPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = CategoriesPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}

	
	public static function doCountJoinArticles(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(CategoriesPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(CategoriesPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(CategoriesPeer::ID_ARTICLES, ArticlesPeer::ID);

		$rs = CategoriesPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinArticles(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		CategoriesPeer::addSelectColumns($c);
		$startcol = (CategoriesPeer::NUM_COLUMNS - CategoriesPeer::NUM_LAZY_LOAD_COLUMNS) + 1;
		ArticlesPeer::addSelectColumns($c);

		$c->addJoin(CategoriesPeer::ID_ARTICLES, ArticlesPeer::ID);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = CategoriesPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = ArticlesPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getArticles(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
										$temp_obj2->addCategories($obj1); 					break;
				}
			}
			if ($newObject) {
				$obj2->initCategoriess();
				$obj2->addCategories($obj1); 			}
			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doCountJoinAll(Criteria $criteria, $distinct = false, $con = null)
	{
		$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(CategoriesPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(CategoriesPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(CategoriesPeer::ID_ARTICLES, ArticlesPeer::ID);

		$rs = CategoriesPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinAll(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		CategoriesPeer::addSelectColumns($c);
		$startcol2 = (CategoriesPeer::NUM_COLUMNS - CategoriesPeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		ArticlesPeer::addSelectColumns($c);
		$startcol3 = $startcol2 + ArticlesPeer::NUM_COLUMNS;

		$c->addJoin(CategoriesPeer::ID_ARTICLES, ArticlesPeer::ID);

		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = CategoriesPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);


					
			$omClass = ArticlesPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getArticles(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addCategories($obj1); 					break;
				}
			}

			if ($newObject) {
				$obj2->initCategoriess();
				$obj2->addCategories($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}

	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return CategoriesPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{

    foreach (sfMixer::getCallables('BaseCategoriesPeer:doInsert:pre') as $callable)
    {
      $ret = call_user_func($callable, 'BaseCategoriesPeer', $values, $con);
      if (false !== $ret)
      {
        return $ret;
      }
    }


		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(CategoriesPeer::ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		
    foreach (sfMixer::getCallables('BaseCategoriesPeer:doInsert:post') as $callable)
    {
      call_user_func($callable, 'BaseCategoriesPeer', $values, $con, $pk);
    }

    return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{

    foreach (sfMixer::getCallables('BaseCategoriesPeer:doUpdate:pre') as $callable)
    {
      $ret = call_user_func($callable, 'BaseCategoriesPeer', $values, $con);
      if (false !== $ret)
      {
        return $ret;
      }
    }


		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(CategoriesPeer::ID);
			$selectCriteria->add(CategoriesPeer::ID, $criteria->remove(CategoriesPeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		$ret = BasePeer::doUpdate($selectCriteria, $criteria, $con);
	

    foreach (sfMixer::getCallables('BaseCategoriesPeer:doUpdate:post') as $callable)
    {
      call_user_func($callable, 'BaseCategoriesPeer', $values, $con, $ret);
    }

    return $ret;
  }

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(CategoriesPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(CategoriesPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof Categories) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(CategoriesPeer::ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(Categories $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(CategoriesPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(CategoriesPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(CategoriesPeer::DATABASE_NAME, CategoriesPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = CategoriesPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(CategoriesPeer::DATABASE_NAME);

		$criteria->add(CategoriesPeer::ID, $pk);


		$v = CategoriesPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(CategoriesPeer::ID, $pks, Criteria::IN);
			$objs = CategoriesPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseCategoriesPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/map/CategoriesMapBuilder.php';
	Propel::registerMapBuilder('lib.model.map.CategoriesMapBuilder');
}
