<?php


abstract class BaseFoto extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $path;


	
	protected $created;


	
	protected $id_species;


	
	protected $id_subgaleries1;


	
	protected $id_galeries;


	
	protected $is_horizontal;


	
	protected $text;


	
	protected $temp1;


	
	protected $temp2;


	
	protected $temp3;


	
	protected $temp4;

	
	protected $aSpecies;

	
	protected $aSubgaleries1;

	
	protected $aGaleries;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getPath()
	{

		return $this->path;
	}

	
	public function getCreated($format = 'Y-m-d H:i:s')
	{

		if ($this->created === null || $this->created === '') {
			return null;
		} elseif (!is_int($this->created)) {
						$ts = strtotime($this->created);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [created] as date/time value: " . var_export($this->created, true));
			}
		} else {
			$ts = $this->created;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getIdSpecies()
	{

		return $this->id_species;
	}

	
	public function getIdSubgaleries1()
	{

		return $this->id_subgaleries1;
	}

	
	public function getIdGaleries()
	{

		return $this->id_galeries;
	}

	
	public function getIsHorizontal()
	{

		return $this->is_horizontal;
	}

	
	public function getText()
	{

		return $this->text;
	}

	
	public function getTemp1()
	{

		return $this->temp1;
	}

	
	public function getTemp2()
	{

		return $this->temp2;
	}

	
	public function getTemp3()
	{

		return $this->temp3;
	}

	
	public function getTemp4()
	{

		return $this->temp4;
	}

	
	public function setId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = FotoPeer::ID;
		}

	} 
	
	public function setPath($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->path !== $v) {
			$this->path = $v;
			$this->modifiedColumns[] = FotoPeer::PATH;
		}

	} 
	
	public function setCreated($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [created] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->created !== $ts) {
			$this->created = $ts;
			$this->modifiedColumns[] = FotoPeer::CREATED;
		}

	} 
	
	public function setIdSpecies($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_species !== $v) {
			$this->id_species = $v;
			$this->modifiedColumns[] = FotoPeer::ID_SPECIES;
		}

		if ($this->aSpecies !== null && $this->aSpecies->getId() !== $v) {
			$this->aSpecies = null;
		}

	} 
	
	public function setIdSubgaleries1($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_subgaleries1 !== $v) {
			$this->id_subgaleries1 = $v;
			$this->modifiedColumns[] = FotoPeer::ID_SUBGALERIES1;
		}

		if ($this->aSubgaleries1 !== null && $this->aSubgaleries1->getId() !== $v) {
			$this->aSubgaleries1 = null;
		}

	} 
	
	public function setIdGaleries($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_galeries !== $v) {
			$this->id_galeries = $v;
			$this->modifiedColumns[] = FotoPeer::ID_GALERIES;
		}

		if ($this->aGaleries !== null && $this->aGaleries->getId() !== $v) {
			$this->aGaleries = null;
		}

	} 
	
	public function setIsHorizontal($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->is_horizontal !== $v) {
			$this->is_horizontal = $v;
			$this->modifiedColumns[] = FotoPeer::IS_HORIZONTAL;
		}

	} 
	
	public function setText($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->text !== $v) {
			$this->text = $v;
			$this->modifiedColumns[] = FotoPeer::TEXT;
		}

	} 
	
	public function setTemp1($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->temp1 !== $v) {
			$this->temp1 = $v;
			$this->modifiedColumns[] = FotoPeer::TEMP1;
		}

	} 
	
	public function setTemp2($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->temp2 !== $v) {
			$this->temp2 = $v;
			$this->modifiedColumns[] = FotoPeer::TEMP2;
		}

	} 
	
	public function setTemp3($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->temp3 !== $v) {
			$this->temp3 = $v;
			$this->modifiedColumns[] = FotoPeer::TEMP3;
		}

	} 
	
	public function setTemp4($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->temp4 !== $v) {
			$this->temp4 = $v;
			$this->modifiedColumns[] = FotoPeer::TEMP4;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->path = $rs->getString($startcol + 1);

			$this->created = $rs->getTimestamp($startcol + 2, null);

			$this->id_species = $rs->getInt($startcol + 3);

			$this->id_subgaleries1 = $rs->getInt($startcol + 4);

			$this->id_galeries = $rs->getInt($startcol + 5);

			$this->is_horizontal = $rs->getString($startcol + 6);

			$this->text = $rs->getString($startcol + 7);

			$this->temp1 = $rs->getString($startcol + 8);

			$this->temp2 = $rs->getString($startcol + 9);

			$this->temp3 = $rs->getString($startcol + 10);

			$this->temp4 = $rs->getString($startcol + 11);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 12; 
		} catch (Exception $e) {
			throw new PropelException("Error populating Foto object", $e);
		}
	}

	
	public function delete($con = null)
	{

    foreach (sfMixer::getCallables('BaseFoto:delete:pre') as $callable)
    {
      $ret = call_user_func($callable, $this, $con);
      if ($ret)
      {
        return;
      }
    }


		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(FotoPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			FotoPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	

    foreach (sfMixer::getCallables('BaseFoto:delete:post') as $callable)
    {
      call_user_func($callable, $this, $con);
    }

  }
	
	public function save($con = null)
	{

    foreach (sfMixer::getCallables('BaseFoto:save:pre') as $callable)
    {
      $affectedRows = call_user_func($callable, $this, $con);
      if (is_int($affectedRows))
      {
        return $affectedRows;
      }
    }


		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(FotoPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
    foreach (sfMixer::getCallables('BaseFoto:save:post') as $callable)
    {
      call_user_func($callable, $this, $con, $affectedRows);
    }

			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


												
			if ($this->aSpecies !== null) {
				if ($this->aSpecies->isModified()) {
					$affectedRows += $this->aSpecies->save($con);
				}
				$this->setSpecies($this->aSpecies);
			}

			if ($this->aSubgaleries1 !== null) {
				if ($this->aSubgaleries1->isModified()) {
					$affectedRows += $this->aSubgaleries1->save($con);
				}
				$this->setSubgaleries1($this->aSubgaleries1);
			}

			if ($this->aGaleries !== null) {
				if ($this->aGaleries->isModified()) {
					$affectedRows += $this->aGaleries->save($con);
				}
				$this->setGaleries($this->aGaleries);
			}


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = FotoPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += FotoPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


												
			if ($this->aSpecies !== null) {
				if (!$this->aSpecies->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aSpecies->getValidationFailures());
				}
			}

			if ($this->aSubgaleries1 !== null) {
				if (!$this->aSubgaleries1->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aSubgaleries1->getValidationFailures());
				}
			}

			if ($this->aGaleries !== null) {
				if (!$this->aGaleries->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aGaleries->getValidationFailures());
				}
			}


			if (($retval = FotoPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = FotoPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getPath();
				break;
			case 2:
				return $this->getCreated();
				break;
			case 3:
				return $this->getIdSpecies();
				break;
			case 4:
				return $this->getIdSubgaleries1();
				break;
			case 5:
				return $this->getIdGaleries();
				break;
			case 6:
				return $this->getIsHorizontal();
				break;
			case 7:
				return $this->getText();
				break;
			case 8:
				return $this->getTemp1();
				break;
			case 9:
				return $this->getTemp2();
				break;
			case 10:
				return $this->getTemp3();
				break;
			case 11:
				return $this->getTemp4();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = FotoPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getPath(),
			$keys[2] => $this->getCreated(),
			$keys[3] => $this->getIdSpecies(),
			$keys[4] => $this->getIdSubgaleries1(),
			$keys[5] => $this->getIdGaleries(),
			$keys[6] => $this->getIsHorizontal(),
			$keys[7] => $this->getText(),
			$keys[8] => $this->getTemp1(),
			$keys[9] => $this->getTemp2(),
			$keys[10] => $this->getTemp3(),
			$keys[11] => $this->getTemp4(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = FotoPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setPath($value);
				break;
			case 2:
				$this->setCreated($value);
				break;
			case 3:
				$this->setIdSpecies($value);
				break;
			case 4:
				$this->setIdSubgaleries1($value);
				break;
			case 5:
				$this->setIdGaleries($value);
				break;
			case 6:
				$this->setIsHorizontal($value);
				break;
			case 7:
				$this->setText($value);
				break;
			case 8:
				$this->setTemp1($value);
				break;
			case 9:
				$this->setTemp2($value);
				break;
			case 10:
				$this->setTemp3($value);
				break;
			case 11:
				$this->setTemp4($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = FotoPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setPath($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setCreated($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setIdSpecies($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setIdSubgaleries1($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setIdGaleries($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setIsHorizontal($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setText($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setTemp1($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setTemp2($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setTemp3($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setTemp4($arr[$keys[11]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(FotoPeer::DATABASE_NAME);

		if ($this->isColumnModified(FotoPeer::ID)) $criteria->add(FotoPeer::ID, $this->id);
		if ($this->isColumnModified(FotoPeer::PATH)) $criteria->add(FotoPeer::PATH, $this->path);
		if ($this->isColumnModified(FotoPeer::CREATED)) $criteria->add(FotoPeer::CREATED, $this->created);
		if ($this->isColumnModified(FotoPeer::ID_SPECIES)) $criteria->add(FotoPeer::ID_SPECIES, $this->id_species);
		if ($this->isColumnModified(FotoPeer::ID_SUBGALERIES1)) $criteria->add(FotoPeer::ID_SUBGALERIES1, $this->id_subgaleries1);
		if ($this->isColumnModified(FotoPeer::ID_GALERIES)) $criteria->add(FotoPeer::ID_GALERIES, $this->id_galeries);
		if ($this->isColumnModified(FotoPeer::IS_HORIZONTAL)) $criteria->add(FotoPeer::IS_HORIZONTAL, $this->is_horizontal);
		if ($this->isColumnModified(FotoPeer::TEXT)) $criteria->add(FotoPeer::TEXT, $this->text);
		if ($this->isColumnModified(FotoPeer::TEMP1)) $criteria->add(FotoPeer::TEMP1, $this->temp1);
		if ($this->isColumnModified(FotoPeer::TEMP2)) $criteria->add(FotoPeer::TEMP2, $this->temp2);
		if ($this->isColumnModified(FotoPeer::TEMP3)) $criteria->add(FotoPeer::TEMP3, $this->temp3);
		if ($this->isColumnModified(FotoPeer::TEMP4)) $criteria->add(FotoPeer::TEMP4, $this->temp4);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(FotoPeer::DATABASE_NAME);

		$criteria->add(FotoPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setPath($this->path);

		$copyObj->setCreated($this->created);

		$copyObj->setIdSpecies($this->id_species);

		$copyObj->setIdSubgaleries1($this->id_subgaleries1);

		$copyObj->setIdGaleries($this->id_galeries);

		$copyObj->setIsHorizontal($this->is_horizontal);

		$copyObj->setText($this->text);

		$copyObj->setTemp1($this->temp1);

		$copyObj->setTemp2($this->temp2);

		$copyObj->setTemp3($this->temp3);

		$copyObj->setTemp4($this->temp4);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new FotoPeer();
		}
		return self::$peer;
	}

	
	public function setSpecies($v)
	{


		if ($v === null) {
			$this->setIdSpecies(NULL);
		} else {
			$this->setIdSpecies($v->getId());
		}


		$this->aSpecies = $v;
	}


	
	public function getSpecies($con = null)
	{
		if ($this->aSpecies === null && ($this->id_species !== null)) {
						include_once 'lib/model/om/BaseSpeciesPeer.php';

			$this->aSpecies = SpeciesPeer::retrieveByPK($this->id_species, $con);

			
		}
		return $this->aSpecies;
	}

	
	public function setSubgaleries1($v)
	{


		if ($v === null) {
			$this->setIdSubgaleries1(NULL);
		} else {
			$this->setIdSubgaleries1($v->getId());
		}


		$this->aSubgaleries1 = $v;
	}


	
	public function getSubgaleries1($con = null)
	{
		if ($this->aSubgaleries1 === null && ($this->id_subgaleries1 !== null)) {
						include_once 'lib/model/om/BaseSubgaleries1Peer.php';

			$this->aSubgaleries1 = Subgaleries1Peer::retrieveByPK($this->id_subgaleries1, $con);

			
		}
		return $this->aSubgaleries1;
	}

	
	public function setGaleries($v)
	{


		if ($v === null) {
			$this->setIdGaleries(NULL);
		} else {
			$this->setIdGaleries($v->getId());
		}


		$this->aGaleries = $v;
	}


	
	public function getGaleries($con = null)
	{
		if ($this->aGaleries === null && ($this->id_galeries !== null)) {
						include_once 'lib/model/om/BaseGaleriesPeer.php';

			$this->aGaleries = GaleriesPeer::retrieveByPK($this->id_galeries, $con);

			
		}
		return $this->aGaleries;
	}


  public function __call($method, $arguments)
  {
    if (!$callable = sfMixer::getCallable('BaseFoto:'.$method))
    {
      throw new sfException(sprintf('Call to undefined method BaseFoto::%s', $method));
    }

    array_unshift($arguments, $this);

    return call_user_func_array($callable, $arguments);
  }


} 