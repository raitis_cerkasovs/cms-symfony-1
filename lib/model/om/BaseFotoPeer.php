<?php


abstract class BaseFotoPeer {

	
	const DATABASE_NAME = 'propel';

	
	const TABLE_NAME = 'foto';

	
	const CLASS_DEFAULT = 'lib.model.Foto';

	
	const NUM_COLUMNS = 12;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID = 'foto.ID';

	
	const PATH = 'foto.PATH';

	
	const CREATED = 'foto.CREATED';

	
	const ID_SPECIES = 'foto.ID_SPECIES';

	
	const ID_SUBGALERIES1 = 'foto.ID_SUBGALERIES1';

	
	const ID_GALERIES = 'foto.ID_GALERIES';

	
	const IS_HORIZONTAL = 'foto.IS_HORIZONTAL';

	
	const TEXT = 'foto.TEXT';

	
	const TEMP1 = 'foto.TEMP1';

	
	const TEMP2 = 'foto.TEMP2';

	
	const TEMP3 = 'foto.TEMP3';

	
	const TEMP4 = 'foto.TEMP4';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Id', 'Path', 'Created', 'IdSpecies', 'IdSubgaleries1', 'IdGaleries', 'IsHorizontal', 'Text', 'Temp1', 'Temp2', 'Temp3', 'Temp4', ),
		BasePeer::TYPE_COLNAME => array (FotoPeer::ID, FotoPeer::PATH, FotoPeer::CREATED, FotoPeer::ID_SPECIES, FotoPeer::ID_SUBGALERIES1, FotoPeer::ID_GALERIES, FotoPeer::IS_HORIZONTAL, FotoPeer::TEXT, FotoPeer::TEMP1, FotoPeer::TEMP2, FotoPeer::TEMP3, FotoPeer::TEMP4, ),
		BasePeer::TYPE_FIELDNAME => array ('id', 'path', 'created', 'id_species', 'id_subgaleries1', 'id_galeries', 'is_horizontal', 'text', 'temp1', 'temp2', 'temp3', 'temp4', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'Path' => 1, 'Created' => 2, 'IdSpecies' => 3, 'IdSubgaleries1' => 4, 'IdGaleries' => 5, 'IsHorizontal' => 6, 'Text' => 7, 'Temp1' => 8, 'Temp2' => 9, 'Temp3' => 10, 'Temp4' => 11, ),
		BasePeer::TYPE_COLNAME => array (FotoPeer::ID => 0, FotoPeer::PATH => 1, FotoPeer::CREATED => 2, FotoPeer::ID_SPECIES => 3, FotoPeer::ID_SUBGALERIES1 => 4, FotoPeer::ID_GALERIES => 5, FotoPeer::IS_HORIZONTAL => 6, FotoPeer::TEXT => 7, FotoPeer::TEMP1 => 8, FotoPeer::TEMP2 => 9, FotoPeer::TEMP3 => 10, FotoPeer::TEMP4 => 11, ),
		BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'path' => 1, 'created' => 2, 'id_species' => 3, 'id_subgaleries1' => 4, 'id_galeries' => 5, 'is_horizontal' => 6, 'text' => 7, 'temp1' => 8, 'temp2' => 9, 'temp3' => 10, 'temp4' => 11, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/map/FotoMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.map.FotoMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = FotoPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(FotoPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(FotoPeer::ID);

		$criteria->addSelectColumn(FotoPeer::PATH);

		$criteria->addSelectColumn(FotoPeer::CREATED);

		$criteria->addSelectColumn(FotoPeer::ID_SPECIES);

		$criteria->addSelectColumn(FotoPeer::ID_SUBGALERIES1);

		$criteria->addSelectColumn(FotoPeer::ID_GALERIES);

		$criteria->addSelectColumn(FotoPeer::IS_HORIZONTAL);

		$criteria->addSelectColumn(FotoPeer::TEXT);

		$criteria->addSelectColumn(FotoPeer::TEMP1);

		$criteria->addSelectColumn(FotoPeer::TEMP2);

		$criteria->addSelectColumn(FotoPeer::TEMP3);

		$criteria->addSelectColumn(FotoPeer::TEMP4);

	}

	const COUNT = 'COUNT(foto.ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT foto.ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(FotoPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(FotoPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = FotoPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = FotoPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return FotoPeer::populateObjects(FotoPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{

    foreach (sfMixer::getCallables('BaseFotoPeer:addDoSelectRS:addDoSelectRS') as $callable)
    {
      call_user_func($callable, 'BaseFotoPeer', $criteria, $con);
    }


		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			FotoPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = FotoPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}

	
	public static function doCountJoinSpecies(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(FotoPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(FotoPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(FotoPeer::ID_SPECIES, SpeciesPeer::ID);

		$rs = FotoPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinSubgaleries1(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(FotoPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(FotoPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(FotoPeer::ID_SUBGALERIES1, Subgaleries1Peer::ID);

		$rs = FotoPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinGaleries(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(FotoPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(FotoPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(FotoPeer::ID_GALERIES, GaleriesPeer::ID);

		$rs = FotoPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinSpecies(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		FotoPeer::addSelectColumns($c);
		$startcol = (FotoPeer::NUM_COLUMNS - FotoPeer::NUM_LAZY_LOAD_COLUMNS) + 1;
		SpeciesPeer::addSelectColumns($c);

		$c->addJoin(FotoPeer::ID_SPECIES, SpeciesPeer::ID);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = FotoPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = SpeciesPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getSpecies(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
										$temp_obj2->addFoto($obj1); 					break;
				}
			}
			if ($newObject) {
				$obj2->initFotos();
				$obj2->addFoto($obj1); 			}
			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinSubgaleries1(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		FotoPeer::addSelectColumns($c);
		$startcol = (FotoPeer::NUM_COLUMNS - FotoPeer::NUM_LAZY_LOAD_COLUMNS) + 1;
		Subgaleries1Peer::addSelectColumns($c);

		$c->addJoin(FotoPeer::ID_SUBGALERIES1, Subgaleries1Peer::ID);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = FotoPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = Subgaleries1Peer::getOMClass();

			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getSubgaleries1(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
										$temp_obj2->addFoto($obj1); 					break;
				}
			}
			if ($newObject) {
				$obj2->initFotos();
				$obj2->addFoto($obj1); 			}
			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinGaleries(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		FotoPeer::addSelectColumns($c);
		$startcol = (FotoPeer::NUM_COLUMNS - FotoPeer::NUM_LAZY_LOAD_COLUMNS) + 1;
		GaleriesPeer::addSelectColumns($c);

		$c->addJoin(FotoPeer::ID_GALERIES, GaleriesPeer::ID);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = FotoPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = GaleriesPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getGaleries(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
										$temp_obj2->addFoto($obj1); 					break;
				}
			}
			if ($newObject) {
				$obj2->initFotos();
				$obj2->addFoto($obj1); 			}
			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doCountJoinAll(Criteria $criteria, $distinct = false, $con = null)
	{
		$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(FotoPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(FotoPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(FotoPeer::ID_SPECIES, SpeciesPeer::ID);

		$criteria->addJoin(FotoPeer::ID_SUBGALERIES1, Subgaleries1Peer::ID);

		$criteria->addJoin(FotoPeer::ID_GALERIES, GaleriesPeer::ID);

		$rs = FotoPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinAll(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		FotoPeer::addSelectColumns($c);
		$startcol2 = (FotoPeer::NUM_COLUMNS - FotoPeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		SpeciesPeer::addSelectColumns($c);
		$startcol3 = $startcol2 + SpeciesPeer::NUM_COLUMNS;

		Subgaleries1Peer::addSelectColumns($c);
		$startcol4 = $startcol3 + Subgaleries1Peer::NUM_COLUMNS;

		GaleriesPeer::addSelectColumns($c);
		$startcol5 = $startcol4 + GaleriesPeer::NUM_COLUMNS;

		$c->addJoin(FotoPeer::ID_SPECIES, SpeciesPeer::ID);

		$c->addJoin(FotoPeer::ID_SUBGALERIES1, Subgaleries1Peer::ID);

		$c->addJoin(FotoPeer::ID_GALERIES, GaleriesPeer::ID);

		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = FotoPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);


					
			$omClass = SpeciesPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getSpecies(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addFoto($obj1); 					break;
				}
			}

			if ($newObject) {
				$obj2->initFotos();
				$obj2->addFoto($obj1);
			}


					
			$omClass = Subgaleries1Peer::getOMClass();


			$cls = Propel::import($omClass);
			$obj3 = new $cls();
			$obj3->hydrate($rs, $startcol3);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj3 = $temp_obj1->getSubgaleries1(); 				if ($temp_obj3->getPrimaryKey() === $obj3->getPrimaryKey()) {
					$newObject = false;
					$temp_obj3->addFoto($obj1); 					break;
				}
			}

			if ($newObject) {
				$obj3->initFotos();
				$obj3->addFoto($obj1);
			}


					
			$omClass = GaleriesPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj4 = new $cls();
			$obj4->hydrate($rs, $startcol4);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj4 = $temp_obj1->getGaleries(); 				if ($temp_obj4->getPrimaryKey() === $obj4->getPrimaryKey()) {
					$newObject = false;
					$temp_obj4->addFoto($obj1); 					break;
				}
			}

			if ($newObject) {
				$obj4->initFotos();
				$obj4->addFoto($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doCountJoinAllExceptSpecies(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(FotoPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(FotoPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(FotoPeer::ID_SUBGALERIES1, Subgaleries1Peer::ID);

		$criteria->addJoin(FotoPeer::ID_GALERIES, GaleriesPeer::ID);

		$rs = FotoPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinAllExceptSubgaleries1(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(FotoPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(FotoPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(FotoPeer::ID_SPECIES, SpeciesPeer::ID);

		$criteria->addJoin(FotoPeer::ID_GALERIES, GaleriesPeer::ID);

		$rs = FotoPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinAllExceptGaleries(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(FotoPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(FotoPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(FotoPeer::ID_SPECIES, SpeciesPeer::ID);

		$criteria->addJoin(FotoPeer::ID_SUBGALERIES1, Subgaleries1Peer::ID);

		$rs = FotoPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinAllExceptSpecies(Criteria $c, $con = null)
	{
		$c = clone $c;

								if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		FotoPeer::addSelectColumns($c);
		$startcol2 = (FotoPeer::NUM_COLUMNS - FotoPeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		Subgaleries1Peer::addSelectColumns($c);
		$startcol3 = $startcol2 + Subgaleries1Peer::NUM_COLUMNS;

		GaleriesPeer::addSelectColumns($c);
		$startcol4 = $startcol3 + GaleriesPeer::NUM_COLUMNS;

		$c->addJoin(FotoPeer::ID_SUBGALERIES1, Subgaleries1Peer::ID);

		$c->addJoin(FotoPeer::ID_GALERIES, GaleriesPeer::ID);


		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = FotoPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = Subgaleries1Peer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2  = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getSubgaleries1(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addFoto($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj2->initFotos();
				$obj2->addFoto($obj1);
			}

			$omClass = GaleriesPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj3  = new $cls();
			$obj3->hydrate($rs, $startcol3);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj3 = $temp_obj1->getGaleries(); 				if ($temp_obj3->getPrimaryKey() === $obj3->getPrimaryKey()) {
					$newObject = false;
					$temp_obj3->addFoto($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj3->initFotos();
				$obj3->addFoto($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinAllExceptSubgaleries1(Criteria $c, $con = null)
	{
		$c = clone $c;

								if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		FotoPeer::addSelectColumns($c);
		$startcol2 = (FotoPeer::NUM_COLUMNS - FotoPeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		SpeciesPeer::addSelectColumns($c);
		$startcol3 = $startcol2 + SpeciesPeer::NUM_COLUMNS;

		GaleriesPeer::addSelectColumns($c);
		$startcol4 = $startcol3 + GaleriesPeer::NUM_COLUMNS;

		$c->addJoin(FotoPeer::ID_SPECIES, SpeciesPeer::ID);

		$c->addJoin(FotoPeer::ID_GALERIES, GaleriesPeer::ID);


		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = FotoPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = SpeciesPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2  = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getSpecies(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addFoto($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj2->initFotos();
				$obj2->addFoto($obj1);
			}

			$omClass = GaleriesPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj3  = new $cls();
			$obj3->hydrate($rs, $startcol3);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj3 = $temp_obj1->getGaleries(); 				if ($temp_obj3->getPrimaryKey() === $obj3->getPrimaryKey()) {
					$newObject = false;
					$temp_obj3->addFoto($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj3->initFotos();
				$obj3->addFoto($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinAllExceptGaleries(Criteria $c, $con = null)
	{
		$c = clone $c;

								if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		FotoPeer::addSelectColumns($c);
		$startcol2 = (FotoPeer::NUM_COLUMNS - FotoPeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		SpeciesPeer::addSelectColumns($c);
		$startcol3 = $startcol2 + SpeciesPeer::NUM_COLUMNS;

		Subgaleries1Peer::addSelectColumns($c);
		$startcol4 = $startcol3 + Subgaleries1Peer::NUM_COLUMNS;

		$c->addJoin(FotoPeer::ID_SPECIES, SpeciesPeer::ID);

		$c->addJoin(FotoPeer::ID_SUBGALERIES1, Subgaleries1Peer::ID);


		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = FotoPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = SpeciesPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2  = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getSpecies(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addFoto($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj2->initFotos();
				$obj2->addFoto($obj1);
			}

			$omClass = Subgaleries1Peer::getOMClass();


			$cls = Propel::import($omClass);
			$obj3  = new $cls();
			$obj3->hydrate($rs, $startcol3);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj3 = $temp_obj1->getSubgaleries1(); 				if ($temp_obj3->getPrimaryKey() === $obj3->getPrimaryKey()) {
					$newObject = false;
					$temp_obj3->addFoto($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj3->initFotos();
				$obj3->addFoto($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}

	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return FotoPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{

    foreach (sfMixer::getCallables('BaseFotoPeer:doInsert:pre') as $callable)
    {
      $ret = call_user_func($callable, 'BaseFotoPeer', $values, $con);
      if (false !== $ret)
      {
        return $ret;
      }
    }


		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(FotoPeer::ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		
    foreach (sfMixer::getCallables('BaseFotoPeer:doInsert:post') as $callable)
    {
      call_user_func($callable, 'BaseFotoPeer', $values, $con, $pk);
    }

    return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{

    foreach (sfMixer::getCallables('BaseFotoPeer:doUpdate:pre') as $callable)
    {
      $ret = call_user_func($callable, 'BaseFotoPeer', $values, $con);
      if (false !== $ret)
      {
        return $ret;
      }
    }


		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(FotoPeer::ID);
			$selectCriteria->add(FotoPeer::ID, $criteria->remove(FotoPeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		$ret = BasePeer::doUpdate($selectCriteria, $criteria, $con);
	

    foreach (sfMixer::getCallables('BaseFotoPeer:doUpdate:post') as $callable)
    {
      call_user_func($callable, 'BaseFotoPeer', $values, $con, $ret);
    }

    return $ret;
  }

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(FotoPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(FotoPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof Foto) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(FotoPeer::ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(Foto $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(FotoPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(FotoPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(FotoPeer::DATABASE_NAME, FotoPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = FotoPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(FotoPeer::DATABASE_NAME);

		$criteria->add(FotoPeer::ID, $pk);


		$v = FotoPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(FotoPeer::ID, $pks, Criteria::IN);
			$objs = FotoPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseFotoPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/map/FotoMapBuilder.php';
	Propel::registerMapBuilder('lib.model.map.FotoMapBuilder');
}
