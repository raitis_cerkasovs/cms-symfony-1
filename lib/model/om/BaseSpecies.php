<?php


abstract class BaseSpecies extends BaseObject  implements Persistent {

	protected $temp1;
	protected $temp2;
	protected $temp3;
	protected $temp4;
		protected $textgoogle;


	
	public function getTextgoogle()
	{

		return $this->textgoogle;
	}

	public function getTemp1()
	{

		return $this->temp1;
	}
	public function getTemp4()
	{

		return $this->temp4;
	}
	
	public function getTemp2()
	{

		return $this->temp2;
	}

	
	public function getTemp3()
	{

		return $this->temp3;
	}
	
		
	public function setTemp1($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->temp1 !== $v) {
			$this->temp1 = $v;
			$this->modifiedColumns[] = SpeciesPeer::TEMP1;
		}

	} 

	
	public function setTemp2($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->temp2 !== $v) {
			$this->temp2 = $v;
			$this->modifiedColumns[] = SpeciesPeer::TEMP2;
		}

	} 

	
	public function setTemp3($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->temp3 !== $v) {
			$this->temp3 = $v;
			$this->modifiedColumns[] = SpeciesPeer::TEMP3;
		}

	} 
	
		
	public function setTemp4($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->temp4 !== $v) {
			$this->temp4 = $v;
			$this->modifiedColumns[] = SpeciesPeer::TEMP4;
		}

	} 
	
	
		public function setTextgoogle($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->textgoogle !== $v) {
			$this->textgoogle = $v;
			$this->modifiedColumns[] = SpeciesPeer::TEXTGOOGLE;
		}

	} 

	
	protected static $peer;


	
	protected $id;


	
	protected $lv;


	
	protected $latin;

	
	protected $collFotos;

	
	protected $lastFotoCriteria = null;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getLv()
	{

		return $this->lv;
	}

	
	public function getLatin()
	{

		return $this->latin;
	}

	
	public function setId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = SpeciesPeer::ID;
		}

	} 
	
	public function setLv($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->lv !== $v) {
			$this->lv = $v;
			$this->modifiedColumns[] = SpeciesPeer::LV;
		}

	} 
	
	public function setLatin($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->latin !== $v) {
			$this->latin = $v;
			$this->modifiedColumns[] = SpeciesPeer::LATIN;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->lv = $rs->getString($startcol + 1);

			$this->latin = $rs->getString($startcol + 2);
						$this->textgoogle = $rs->getString($startcol + 3);

			$this->temp4 = $rs->getString($startcol + 4);

			$this->temp1 = $rs->getString($startcol + 5);

			$this->temp2 = $rs->getString($startcol + 6);

			$this->temp3 = $rs->getString($startcol + 7);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 3; 
		} catch (Exception $e) {
			throw new PropelException("Error populating Species object", $e);
		}
	}

	
	public function delete($con = null)
	{

    foreach (sfMixer::getCallables('BaseSpecies:delete:pre') as $callable)
    {
      $ret = call_user_func($callable, $this, $con);
      if ($ret)
      {
        return;
      }
    }


		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(SpeciesPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			SpeciesPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	

    foreach (sfMixer::getCallables('BaseSpecies:delete:post') as $callable)
    {
      call_user_func($callable, $this, $con);
    }

  }
	
	public function save($con = null)
	{

    foreach (sfMixer::getCallables('BaseSpecies:save:pre') as $callable)
    {
      $affectedRows = call_user_func($callable, $this, $con);
      if (is_int($affectedRows))
      {
        return $affectedRows;
      }
    }


		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(SpeciesPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
    foreach (sfMixer::getCallables('BaseSpecies:save:post') as $callable)
    {
      call_user_func($callable, $this, $con, $affectedRows);
    }

			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = SpeciesPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += SpeciesPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			if ($this->collFotos !== null) {
				foreach($this->collFotos as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = SpeciesPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collFotos !== null) {
					foreach($this->collFotos as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = SpeciesPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getLv();
				break;
			case 2:
				return $this->getLatin();
				break;
							case 3:
				return $this->getTextgoogle();
				break;
			case 4:
				return $this->getTemp4();
				break;
			case 5:
				return $this->getTemp1();
				break;
			case 6:
				return $this->getTemp2();
				break;
			case 7:
				return $this->getTemp3();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = SpeciesPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getLv(),
			$keys[2] => $this->getLatin(),
						$keys[3] => $this->getTextgoogle(),
			$keys[4] => $this->getTemp4(),
			$keys[5] => $this->getTemp1(),
			$keys[6] => $this->getTemp2(),
			$keys[7] => $this->getTemp3(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = SpeciesPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setLv($value);
				break;
			case 2:
				$this->setLatin($value);
				break;
							case 3:
				$this->setTextgoogle($value);
				break;
			case 4:
				$this->setTemp4($value);
				break;
			case 5:
				$this->setTemp1($value);
				break;
			case 6:
				$this->setTemp2($value);
				break;
			case 7:
				$this->setTemp3($value);
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = SpeciesPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setLv($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setLatin($arr[$keys[2]]);
				if (array_key_exists($keys[3], $arr)) $this->setTextgoogle($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setTemp4($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setTemp1($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setTemp2($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setTemp3($arr[$keys[7]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(SpeciesPeer::DATABASE_NAME);

		if ($this->isColumnModified(SpeciesPeer::ID)) $criteria->add(SpeciesPeer::ID, $this->id);
		if ($this->isColumnModified(SpeciesPeer::LV)) $criteria->add(SpeciesPeer::LV, $this->lv);
		if ($this->isColumnModified(SpeciesPeer::LATIN)) $criteria->add(SpeciesPeer::LATIN, $this->latin);
	if ($this->isColumnModified(SpeciesPeer::TEXTGOOGLE)) $criteria->add(SpeciesPeer::TEXTGOOGLE, $this->textgoogle);
		if ($this->isColumnModified(SpeciesPeer::TEMP4)) $criteria->add(SpeciesPeer::TEMP4, $this->temp4);
		if ($this->isColumnModified(SpeciesPeer::TEMP1)) $criteria->add(SpeciesPeer::TEMP1, $this->temp1);
		if ($this->isColumnModified(SpeciesPeer::TEMP2)) $criteria->add(SpeciesPeer::TEMP2, $this->temp2);
		if ($this->isColumnModified(SpeciesPeer::TEMP3)) $criteria->add(SpeciesPeer::TEMP3, $this->temp3);
		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(SpeciesPeer::DATABASE_NAME);

		$criteria->add(SpeciesPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setLv($this->lv);

		$copyObj->setLatin($this->latin);
				$copyObj->setTextgoogle($this->textgoogle);

		$copyObj->setTemp4($this->temp4);

		$copyObj->setTemp1($this->temp1);

		$copyObj->setTemp2($this->temp2);

		$copyObj->setTemp3($this->temp3);


		if ($deepCopy) {
									$copyObj->setNew(false);

			foreach($this->getFotos() as $relObj) {
				$copyObj->addFoto($relObj->copy($deepCopy));
			}
			////////////////
			
						foreach($this->getGoogles() as $relObj) {
				$copyObj->addGoogle($relObj->copy($deepCopy));
			}
			//////////////////

		} 

		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new SpeciesPeer();
		}
		return self::$peer;
	}

	
	public function initFotos()
	{
		if ($this->collFotos === null) {
			$this->collFotos = array();
		}
	}

	
	public function getFotos($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseFotoPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collFotos === null) {
			if ($this->isNew()) {
			   $this->collFotos = array();
			} else {

				$criteria->add(FotoPeer::ID_SPECIES, $this->getId());

				FotoPeer::addSelectColumns($criteria);
				$this->collFotos = FotoPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(FotoPeer::ID_SPECIES, $this->getId());

				FotoPeer::addSelectColumns($criteria);
				if (!isset($this->lastFotoCriteria) || !$this->lastFotoCriteria->equals($criteria)) {
					$this->collFotos = FotoPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastFotoCriteria = $criteria;
		return $this->collFotos;
	}

	
	public function countFotos($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseFotoPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(FotoPeer::ID_SPECIES, $this->getId());

		return FotoPeer::doCount($criteria, $distinct, $con);
	}
	
		public function countGoogles($criteria = null, $distinct = false, $con = null)
	{
		
		include_once 'lib/model/om/BaseGooglePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(GooglePeer::IDSPECIES, $this->getId());

		return GooglePeer::doCount($criteria, $distinct, $con);
	}

	
	public function addFoto(Foto $l)
	{
		$this->collFotos[] = $l;
		$l->setSpecies($this);
	}


	
	public function getFotosJoinSubgaleries1($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseFotoPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collFotos === null) {
			if ($this->isNew()) {
				$this->collFotos = array();
			} else {

				$criteria->add(FotoPeer::ID_SPECIES, $this->getId());

				$this->collFotos = FotoPeer::doSelectJoinSubgaleries1($criteria, $con);
			}
		} else {
									
			$criteria->add(FotoPeer::ID_SPECIES, $this->getId());

			if (!isset($this->lastFotoCriteria) || !$this->lastFotoCriteria->equals($criteria)) {
				$this->collFotos = FotoPeer::doSelectJoinSubgaleries1($criteria, $con);
			}
		}
		$this->lastFotoCriteria = $criteria;

		return $this->collFotos;
	}


	
	public function getFotosJoinGaleries($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseFotoPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collFotos === null) {
			if ($this->isNew()) {
				$this->collFotos = array();
			} else {

				$criteria->add(FotoPeer::ID_SPECIES, $this->getId());

				$this->collFotos = FotoPeer::doSelectJoinGaleries($criteria, $con);
			}
		} else {
									
			$criteria->add(FotoPeer::ID_SPECIES, $this->getId());

			if (!isset($this->lastFotoCriteria) || !$this->lastFotoCriteria->equals($criteria)) {
				$this->collFotos = FotoPeer::doSelectJoinGaleries($criteria, $con);
			}
		}
		$this->lastFotoCriteria = $criteria;

		return $this->collFotos;
	}


  public function __call($method, $arguments)
  {
    if (!$callable = sfMixer::getCallable('BaseSpecies:'.$method))
    {
      throw new sfException(sprintf('Call to undefined method BaseSpecies::%s', $method));
    }

    array_unshift($arguments, $this);

    return call_user_func_array($callable, $arguments);
  }
  public function getGoogles($criteria = null, $con = null)
	{
		
		include_once 'lib/model/om/BaseGooglePeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collGoogles === null) {
			if ($this->isNew()) {
			   $this->collGoogles = array();
			} else {

				$criteria->add(GooglePeer::IDSPECIES, $this->getId());

				GooglePeer::addSelectColumns($criteria);
				$this->collGoogles = GooglePeer::doSelect($criteria, $con);
			}
		} else {
			
			if (!$this->isNew()) {
				
				
				


				$criteria->add(GooglePeer::IDSPECIES, $this->getId());

				GooglePeer::addSelectColumns($criteria);
				if (!isset($this->lastGoogleCriteria) || !$this->lastGoogleCriteria->equals($criteria)) {
					$this->collGoogles = GooglePeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastGoogleCriteria = $criteria;
		return $this->collGoogles;
	}


} 