<?php


abstract class BaseSpeciesPeer {

	
	const DATABASE_NAME = 'propel';

	
	const TABLE_NAME = 'species';

	
	const CLASS_DEFAULT = 'lib.model.Species';

	
	const NUM_COLUMNS = 3;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID = 'species.ID';

	
	const LV = 'species.LV';

	
	const LATIN = 'species.LATIN';
	const TEXTGOOGLE = 'species.TEXTGOOGLE';

	
	const TEMP4 = 'species.TEMP4';

	
	const TEMP1 = 'species.TEMP1';

	
	const TEMP2 = 'species.TEMP2';

	
	const TEMP3 = 'species.TEMP3';


	
	private static $phpNameMap = null;


	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Id', 'Lv', 'Latin', 'Textgoogle', 'Temp4', 'Temp1', 'Temp2', 'Temp3', ),
		BasePeer::TYPE_COLNAME => array (SpeciesPeer::ID, SpeciesPeer::LV, SpeciesPeer::LATIN, SpeciesPeer::TEXTGOOGLE, SpeciesPeer::TEMP4, SpeciesPeer::TEMP1, SpeciesPeer::TEMP2, SpeciesPeer::TEMP3, ),
		BasePeer::TYPE_FIELDNAME => array ('id', 'lv', 'latin', 'textGoogle', 'temp4', 'temp1', 'temp2', 'temp3', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'Lv' => 1, 'Latin' => 2, 'Textgoogle' => 3, 'Temp4' => 4, 'Temp1' => 5, 'Temp2' => 6, 'Temp3' => 7, ),
		BasePeer::TYPE_COLNAME => array (SpeciesPeer::ID => 0, SpeciesPeer::LV => 1, SpeciesPeer::LATIN => 2, SpeciesPeer::TEXTGOOGLE => 3, SpeciesPeer::TEMP4 => 4, SpeciesPeer::TEMP1 => 5, SpeciesPeer::TEMP2 => 6, SpeciesPeer::TEMP3 => 7, ),
		BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'lv' => 1, 'latin' => 2, 'textGoogle' => 3, 'temp4' => 4, 'temp1' => 5, 'temp2' => 6, 'temp3' => 7, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/map/SpeciesMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.map.SpeciesMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = SpeciesPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(SpeciesPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(SpeciesPeer::ID);

		$criteria->addSelectColumn(SpeciesPeer::LV);

		$criteria->addSelectColumn(SpeciesPeer::LATIN);
			$criteria->addSelectColumn(SpeciesPeer::TEXTGOOGLE);

		$criteria->addSelectColumn(SpeciesPeer::TEMP4);

		$criteria->addSelectColumn(SpeciesPeer::TEMP1);

		$criteria->addSelectColumn(SpeciesPeer::TEMP2);

		$criteria->addSelectColumn(SpeciesPeer::TEMP3);

	}

	const COUNT = 'COUNT(species.ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT species.ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(SpeciesPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(SpeciesPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = SpeciesPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = SpeciesPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return SpeciesPeer::populateObjects(SpeciesPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{

    foreach (sfMixer::getCallables('BaseSpeciesPeer:addDoSelectRS:addDoSelectRS') as $callable)
    {
      call_user_func($callable, 'BaseSpeciesPeer', $criteria, $con);
    }


		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			SpeciesPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = SpeciesPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return SpeciesPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{

    foreach (sfMixer::getCallables('BaseSpeciesPeer:doInsert:pre') as $callable)
    {
      $ret = call_user_func($callable, 'BaseSpeciesPeer', $values, $con);
      if (false !== $ret)
      {
        return $ret;
      }
    }


		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(SpeciesPeer::ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		
    foreach (sfMixer::getCallables('BaseSpeciesPeer:doInsert:post') as $callable)
    {
      call_user_func($callable, 'BaseSpeciesPeer', $values, $con, $pk);
    }

    return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{

    foreach (sfMixer::getCallables('BaseSpeciesPeer:doUpdate:pre') as $callable)
    {
      $ret = call_user_func($callable, 'BaseSpeciesPeer', $values, $con);
      if (false !== $ret)
      {
        return $ret;
      }
    }


		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(SpeciesPeer::ID);
			$selectCriteria->add(SpeciesPeer::ID, $criteria->remove(SpeciesPeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		$ret = BasePeer::doUpdate($selectCriteria, $criteria, $con);
	

    foreach (sfMixer::getCallables('BaseSpeciesPeer:doUpdate:post') as $callable)
    {
      call_user_func($callable, 'BaseSpeciesPeer', $values, $con, $ret);
    }

    return $ret;
  }

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(SpeciesPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(SpeciesPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof Species) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(SpeciesPeer::ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(Species $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(SpeciesPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(SpeciesPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(SpeciesPeer::DATABASE_NAME, SpeciesPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = SpeciesPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(SpeciesPeer::DATABASE_NAME);

		$criteria->add(SpeciesPeer::ID, $pk);


		$v = SpeciesPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(SpeciesPeer::ID, $pks, Criteria::IN);
			$objs = SpeciesPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseSpeciesPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/map/SpeciesMapBuilder.php';
	Propel::registerMapBuilder('lib.model.map.SpeciesMapBuilder');
}
