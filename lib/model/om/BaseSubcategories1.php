<?php


abstract class BaseSubcategories1 extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $id_categories;


	
	protected $id_articles;


	
	protected $name;


	
	protected $extmodule;


	
	protected $parent_id;


	
	protected $position;


	
	protected $lang_id;


	
	protected $status;


	
	protected $isvert;

	
	protected $aCategories;

	
	protected $aArticles;

	
	protected $collSubcategories2s;

	
	protected $lastSubcategories2Criteria = null;

	
	protected $collBunchs;

	
	protected $lastBunchCriteria = null;

	
	protected $collFotos;

	
	protected $lastFotoCriteria = null;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getIdCategories()
	{

		return $this->id_categories;
	}

	
	public function getIdArticles()
	{

		return $this->id_articles;
	}

	
	public function getName()
	{

		return $this->name;
	}

	
	public function getExtmodule()
	{

		return $this->extmodule;
	}

	
	public function getParentId()
	{

		return $this->parent_id;
	}

	
	public function getPosition()
	{

		return $this->position;
	}

	
	public function getLangId()
	{

		return $this->lang_id;
	}

	
	public function getStatus()
	{

		return $this->status;
	}

	
	public function getIsvert()
	{

		return $this->isvert;
	}

	
	public function setId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = Subcategories1Peer::ID;
		}

	} 
	
	public function setIdCategories($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_categories !== $v) {
			$this->id_categories = $v;
			$this->modifiedColumns[] = Subcategories1Peer::ID_CATEGORIES;
		}

		if ($this->aCategories !== null && $this->aCategories->getId() !== $v) {
			$this->aCategories = null;
		}

	} 
	
	public function setIdArticles($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_articles !== $v) {
			$this->id_articles = $v;
			$this->modifiedColumns[] = Subcategories1Peer::ID_ARTICLES;
		}

		if ($this->aArticles !== null && $this->aArticles->getId() !== $v) {
			$this->aArticles = null;
		}

	} 
	
	public function setName($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->name !== $v) {
			$this->name = $v;
			$this->modifiedColumns[] = Subcategories1Peer::NAME;
		}

	} 
	
	public function setExtmodule($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->extmodule !== $v) {
			$this->extmodule = $v;
			$this->modifiedColumns[] = Subcategories1Peer::EXTMODULE;
		}

	} 
	
	public function setParentId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->parent_id !== $v) {
			$this->parent_id = $v;
			$this->modifiedColumns[] = Subcategories1Peer::PARENT_ID;
		}

	} 
	
	public function setPosition($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->position !== $v) {
			$this->position = $v;
			$this->modifiedColumns[] = Subcategories1Peer::POSITION;
		}

	} 
	
	public function setLangId($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->lang_id !== $v) {
			$this->lang_id = $v;
			$this->modifiedColumns[] = Subcategories1Peer::LANG_ID;
		}

	} 
	
	public function setStatus($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->status !== $v) {
			$this->status = $v;
			$this->modifiedColumns[] = Subcategories1Peer::STATUS;
		}

	} 
	
	public function setIsvert($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->isvert !== $v) {
			$this->isvert = $v;
			$this->modifiedColumns[] = Subcategories1Peer::ISVERT;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->id_categories = $rs->getInt($startcol + 1);

			$this->id_articles = $rs->getInt($startcol + 2);

			$this->name = $rs->getString($startcol + 3);

			$this->extmodule = $rs->getString($startcol + 4);

			$this->parent_id = $rs->getInt($startcol + 5);

			$this->position = $rs->getInt($startcol + 6);

			$this->lang_id = $rs->getString($startcol + 7);

			$this->status = $rs->getInt($startcol + 8);

			$this->isvert = $rs->getInt($startcol + 9);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 10; 
		} catch (Exception $e) {
			throw new PropelException("Error populating Subcategories1 object", $e);
		}
	}

	
	public function delete($con = null)
	{

    foreach (sfMixer::getCallables('BaseSubcategories1:delete:pre') as $callable)
    {
      $ret = call_user_func($callable, $this, $con);
      if ($ret)
      {
        return;
      }
    }


		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(Subcategories1Peer::DATABASE_NAME);
		}

		try {
			$con->begin();
			Subcategories1Peer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	

    foreach (sfMixer::getCallables('BaseSubcategories1:delete:post') as $callable)
    {
      call_user_func($callable, $this, $con);
    }

  }
	
	public function save($con = null)
	{

    foreach (sfMixer::getCallables('BaseSubcategories1:save:pre') as $callable)
    {
      $affectedRows = call_user_func($callable, $this, $con);
      if (is_int($affectedRows))
      {
        return $affectedRows;
      }
    }


		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(Subcategories1Peer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
    foreach (sfMixer::getCallables('BaseSubcategories1:save:post') as $callable)
    {
      call_user_func($callable, $this, $con, $affectedRows);
    }

			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


												
			if ($this->aCategories !== null) {
				if ($this->aCategories->isModified()) {
					$affectedRows += $this->aCategories->save($con);
				}
				$this->setCategories($this->aCategories);
			}

			if ($this->aArticles !== null) {
				if ($this->aArticles->isModified()) {
					$affectedRows += $this->aArticles->save($con);
				}
				$this->setArticles($this->aArticles);
			}


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = Subcategories1Peer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += Subcategories1Peer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			if ($this->collSubcategories2s !== null) {
				foreach($this->collSubcategories2s as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collBunchs !== null) {
				foreach($this->collBunchs as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collFotos !== null) {
				foreach($this->collFotos as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


												
			if ($this->aCategories !== null) {
				if (!$this->aCategories->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aCategories->getValidationFailures());
				}
			}

			if ($this->aArticles !== null) {
				if (!$this->aArticles->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aArticles->getValidationFailures());
				}
			}


			if (($retval = Subcategories1Peer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collSubcategories2s !== null) {
					foreach($this->collSubcategories2s as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collBunchs !== null) {
					foreach($this->collBunchs as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collFotos !== null) {
					foreach($this->collFotos as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = Subcategories1Peer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getIdCategories();
				break;
			case 2:
				return $this->getIdArticles();
				break;
			case 3:
				return $this->getName();
				break;
			case 4:
				return $this->getExtmodule();
				break;
			case 5:
				return $this->getParentId();
				break;
			case 6:
				return $this->getPosition();
				break;
			case 7:
				return $this->getLangId();
				break;
			case 8:
				return $this->getStatus();
				break;
			case 9:
				return $this->getIsvert();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = Subcategories1Peer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getIdCategories(),
			$keys[2] => $this->getIdArticles(),
			$keys[3] => $this->getName(),
			$keys[4] => $this->getExtmodule(),
			$keys[5] => $this->getParentId(),
			$keys[6] => $this->getPosition(),
			$keys[7] => $this->getLangId(),
			$keys[8] => $this->getStatus(),
			$keys[9] => $this->getIsvert(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = Subcategories1Peer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setIdCategories($value);
				break;
			case 2:
				$this->setIdArticles($value);
				break;
			case 3:
				$this->setName($value);
				break;
			case 4:
				$this->setExtmodule($value);
				break;
			case 5:
				$this->setParentId($value);
				break;
			case 6:
				$this->setPosition($value);
				break;
			case 7:
				$this->setLangId($value);
				break;
			case 8:
				$this->setStatus($value);
				break;
			case 9:
				$this->setIsvert($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = Subcategories1Peer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setIdCategories($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setIdArticles($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setName($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setExtmodule($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setParentId($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setPosition($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setLangId($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setStatus($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setIsvert($arr[$keys[9]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(Subcategories1Peer::DATABASE_NAME);

		if ($this->isColumnModified(Subcategories1Peer::ID)) $criteria->add(Subcategories1Peer::ID, $this->id);
		if ($this->isColumnModified(Subcategories1Peer::ID_CATEGORIES)) $criteria->add(Subcategories1Peer::ID_CATEGORIES, $this->id_categories);
		if ($this->isColumnModified(Subcategories1Peer::ID_ARTICLES)) $criteria->add(Subcategories1Peer::ID_ARTICLES, $this->id_articles);
		if ($this->isColumnModified(Subcategories1Peer::NAME)) $criteria->add(Subcategories1Peer::NAME, $this->name);
		if ($this->isColumnModified(Subcategories1Peer::EXTMODULE)) $criteria->add(Subcategories1Peer::EXTMODULE, $this->extmodule);
		if ($this->isColumnModified(Subcategories1Peer::PARENT_ID)) $criteria->add(Subcategories1Peer::PARENT_ID, $this->parent_id);
		if ($this->isColumnModified(Subcategories1Peer::POSITION)) $criteria->add(Subcategories1Peer::POSITION, $this->position);
		if ($this->isColumnModified(Subcategories1Peer::LANG_ID)) $criteria->add(Subcategories1Peer::LANG_ID, $this->lang_id);
		if ($this->isColumnModified(Subcategories1Peer::STATUS)) $criteria->add(Subcategories1Peer::STATUS, $this->status);
		if ($this->isColumnModified(Subcategories1Peer::ISVERT)) $criteria->add(Subcategories1Peer::ISVERT, $this->isvert);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(Subcategories1Peer::DATABASE_NAME);

		$criteria->add(Subcategories1Peer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setIdCategories($this->id_categories);

		$copyObj->setIdArticles($this->id_articles);

		$copyObj->setName($this->name);

		$copyObj->setExtmodule($this->extmodule);

		$copyObj->setParentId($this->parent_id);

		$copyObj->setPosition($this->position);

		$copyObj->setLangId($this->lang_id);

		$copyObj->setStatus($this->status);

		$copyObj->setIsvert($this->isvert);


		if ($deepCopy) {
									$copyObj->setNew(false);

			foreach($this->getSubcategories2s() as $relObj) {
				$copyObj->addSubcategories2($relObj->copy($deepCopy));
			}

			foreach($this->getBunchs() as $relObj) {
				$copyObj->addBunch($relObj->copy($deepCopy));
			}

			foreach($this->getFotos() as $relObj) {
				$copyObj->addFoto($relObj->copy($deepCopy));
			}

		} 

		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new Subcategories1Peer();
		}
		return self::$peer;
	}

	
	public function setCategories($v)
	{


		if ($v === null) {
			$this->setIdCategories(NULL);
		} else {
			$this->setIdCategories($v->getId());
		}


		$this->aCategories = $v;
	}


	
	public function getCategories($con = null)
	{
		if ($this->aCategories === null && ($this->id_categories !== null)) {
						include_once 'lib/model/om/BaseCategoriesPeer.php';

			$this->aCategories = CategoriesPeer::retrieveByPK($this->id_categories, $con);

			
		}
		return $this->aCategories;
	}

	
	public function setArticles($v)
	{


		if ($v === null) {
			$this->setIdArticles(NULL);
		} else {
			$this->setIdArticles($v->getId());
		}


		$this->aArticles = $v;
	}


	
	public function getArticles($con = null)
	{
		if ($this->aArticles === null && ($this->id_articles !== null)) {
						include_once 'lib/model/om/BaseArticlesPeer.php';

			$this->aArticles = ArticlesPeer::retrieveByPK($this->id_articles, $con);

			
		}
		return $this->aArticles;
	}

	
	public function initSubcategories2s()
	{
		if ($this->collSubcategories2s === null) {
			$this->collSubcategories2s = array();
		}
	}

	
	public function getSubcategories2s($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseSubcategories2Peer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collSubcategories2s === null) {
			if ($this->isNew()) {
			   $this->collSubcategories2s = array();
			} else {

				$criteria->add(Subcategories2Peer::ID_SUBCATEGORIES1, $this->getId());

				Subcategories2Peer::addSelectColumns($criteria);
				$this->collSubcategories2s = Subcategories2Peer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(Subcategories2Peer::ID_SUBCATEGORIES1, $this->getId());

				Subcategories2Peer::addSelectColumns($criteria);
				if (!isset($this->lastSubcategories2Criteria) || !$this->lastSubcategories2Criteria->equals($criteria)) {
					$this->collSubcategories2s = Subcategories2Peer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastSubcategories2Criteria = $criteria;
		return $this->collSubcategories2s;
	}

	
	public function countSubcategories2s($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseSubcategories2Peer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(Subcategories2Peer::ID_SUBCATEGORIES1, $this->getId());

		return Subcategories2Peer::doCount($criteria, $distinct, $con);
	}

	
	public function addSubcategories2(Subcategories2 $l)
	{
		$this->collSubcategories2s[] = $l;
		$l->setSubcategories1($this);
	}


	
	public function getSubcategories2sJoinArticles($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseSubcategories2Peer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collSubcategories2s === null) {
			if ($this->isNew()) {
				$this->collSubcategories2s = array();
			} else {

				$criteria->add(Subcategories2Peer::ID_SUBCATEGORIES1, $this->getId());

				$this->collSubcategories2s = Subcategories2Peer::doSelectJoinArticles($criteria, $con);
			}
		} else {
									
			$criteria->add(Subcategories2Peer::ID_SUBCATEGORIES1, $this->getId());

			if (!isset($this->lastSubcategories2Criteria) || !$this->lastSubcategories2Criteria->equals($criteria)) {
				$this->collSubcategories2s = Subcategories2Peer::doSelectJoinArticles($criteria, $con);
			}
		}
		$this->lastSubcategories2Criteria = $criteria;

		return $this->collSubcategories2s;
	}

	
	public function initBunchs()
	{
		if ($this->collBunchs === null) {
			$this->collBunchs = array();
		}
	}

	
	public function getBunchs($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseBunchPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collBunchs === null) {
			if ($this->isNew()) {
			   $this->collBunchs = array();
			} else {

				$criteria->add(BunchPeer::IDSUBCAT1, $this->getId());

				BunchPeer::addSelectColumns($criteria);
				$this->collBunchs = BunchPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(BunchPeer::IDSUBCAT1, $this->getId());

				BunchPeer::addSelectColumns($criteria);
				if (!isset($this->lastBunchCriteria) || !$this->lastBunchCriteria->equals($criteria)) {
					$this->collBunchs = BunchPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastBunchCriteria = $criteria;
		return $this->collBunchs;
	}

	
	public function countBunchs($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseBunchPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(BunchPeer::IDSUBCAT1, $this->getId());

		return BunchPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addBunch(Bunch $l)
	{
		$this->collBunchs[] = $l;
		$l->setSubcategories1($this);
	}


	
	public function getBunchsJoinCategories($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseBunchPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collBunchs === null) {
			if ($this->isNew()) {
				$this->collBunchs = array();
			} else {

				$criteria->add(BunchPeer::IDSUBCAT1, $this->getId());

				$this->collBunchs = BunchPeer::doSelectJoinCategories($criteria, $con);
			}
		} else {
									
			$criteria->add(BunchPeer::IDSUBCAT1, $this->getId());

			if (!isset($this->lastBunchCriteria) || !$this->lastBunchCriteria->equals($criteria)) {
				$this->collBunchs = BunchPeer::doSelectJoinCategories($criteria, $con);
			}
		}
		$this->lastBunchCriteria = $criteria;

		return $this->collBunchs;
	}


	
	public function getBunchsJoinFoto($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseBunchPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collBunchs === null) {
			if ($this->isNew()) {
				$this->collBunchs = array();
			} else {

				$criteria->add(BunchPeer::IDSUBCAT1, $this->getId());

				$this->collBunchs = BunchPeer::doSelectJoinFoto($criteria, $con);
			}
		} else {
									
			$criteria->add(BunchPeer::IDSUBCAT1, $this->getId());

			if (!isset($this->lastBunchCriteria) || !$this->lastBunchCriteria->equals($criteria)) {
				$this->collBunchs = BunchPeer::doSelectJoinFoto($criteria, $con);
			}
		}
		$this->lastBunchCriteria = $criteria;

		return $this->collBunchs;
	}

	
	public function initFotos()
	{
		if ($this->collFotos === null) {
			$this->collFotos = array();
		}
	}

	
	public function getFotos($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseFotoPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collFotos === null) {
			if ($this->isNew()) {
			   $this->collFotos = array();
			} else {

				$criteria->add(FotoPeer::IDSLUD, $this->getId());

				FotoPeer::addSelectColumns($criteria);
				$this->collFotos = FotoPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(FotoPeer::IDSLUD, $this->getId());

				FotoPeer::addSelectColumns($criteria);
				if (!isset($this->lastFotoCriteria) || !$this->lastFotoCriteria->equals($criteria)) {
					$this->collFotos = FotoPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastFotoCriteria = $criteria;
		return $this->collFotos;
	}

	
	public function countFotos($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseFotoPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(FotoPeer::IDSLUD, $this->getId());

		return FotoPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addFoto(Foto $l)
	{
		$this->collFotos[] = $l;
		$l->setSubcategories1($this);
	}


	
	public function getFotosJoinCategories($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseFotoPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collFotos === null) {
			if ($this->isNew()) {
				$this->collFotos = array();
			} else {

				$criteria->add(FotoPeer::IDSLUD, $this->getId());

				$this->collFotos = FotoPeer::doSelectJoinCategories($criteria, $con);
			}
		} else {
									
			$criteria->add(FotoPeer::IDSLUD, $this->getId());

			if (!isset($this->lastFotoCriteria) || !$this->lastFotoCriteria->equals($criteria)) {
				$this->collFotos = FotoPeer::doSelectJoinCategories($criteria, $con);
			}
		}
		$this->lastFotoCriteria = $criteria;

		return $this->collFotos;
	}


  public function __call($method, $arguments)
  {
    if (!$callable = sfMixer::getCallable('BaseSubcategories1:'.$method))
    {
      throw new sfException(sprintf('Call to undefined method BaseSubcategories1::%s', $method));
    }

    array_unshift($arguments, $this);

    return call_user_func_array($callable, $arguments);
  }


} 