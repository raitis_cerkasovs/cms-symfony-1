<?php


abstract class BaseSubcategories2 extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $id_subcategories1;


	
	protected $id_articles;


	
	protected $name;


	
	protected $extmodule;


	
	protected $parent_id;


	
	protected $position;


	
	protected $lang_id;


	
	protected $status;


	
	protected $isvert;

	
	protected $aSubcategories1;

	
	protected $aArticles;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getIdSubcategories1()
	{

		return $this->id_subcategories1;
	}

	
	public function getIdArticles()
	{

		return $this->id_articles;
	}

	
	public function getName()
	{

		return $this->name;
	}

	
	public function getExtmodule()
	{

		return $this->extmodule;
	}

	
	public function getParentId()
	{

		return $this->parent_id;
	}

	
	public function getPosition()
	{

		return $this->position;
	}

	
	public function getLangId()
	{

		return $this->lang_id;
	}

	
	public function getStatus()
	{

		return $this->status;
	}

	
	public function getIsvert()
	{

		return $this->isvert;
	}

	
	public function setId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = Subcategories2Peer::ID;
		}

	} 
	
	public function setIdSubcategories1($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_subcategories1 !== $v) {
			$this->id_subcategories1 = $v;
			$this->modifiedColumns[] = Subcategories2Peer::ID_SUBCATEGORIES1;
		}

		if ($this->aSubcategories1 !== null && $this->aSubcategories1->getId() !== $v) {
			$this->aSubcategories1 = null;
		}

	} 
	
	public function setIdArticles($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_articles !== $v) {
			$this->id_articles = $v;
			$this->modifiedColumns[] = Subcategories2Peer::ID_ARTICLES;
		}

		if ($this->aArticles !== null && $this->aArticles->getId() !== $v) {
			$this->aArticles = null;
		}

	} 
	
	public function setName($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->name !== $v) {
			$this->name = $v;
			$this->modifiedColumns[] = Subcategories2Peer::NAME;
		}

	} 
	
	public function setExtmodule($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->extmodule !== $v) {
			$this->extmodule = $v;
			$this->modifiedColumns[] = Subcategories2Peer::EXTMODULE;
		}

	} 
	
	public function setParentId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->parent_id !== $v) {
			$this->parent_id = $v;
			$this->modifiedColumns[] = Subcategories2Peer::PARENT_ID;
		}

	} 
	
	public function setPosition($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->position !== $v) {
			$this->position = $v;
			$this->modifiedColumns[] = Subcategories2Peer::POSITION;
		}

	} 
	
	public function setLangId($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->lang_id !== $v) {
			$this->lang_id = $v;
			$this->modifiedColumns[] = Subcategories2Peer::LANG_ID;
		}

	} 
	
	public function setStatus($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->status !== $v) {
			$this->status = $v;
			$this->modifiedColumns[] = Subcategories2Peer::STATUS;
		}

	} 
	
	public function setIsvert($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->isvert !== $v) {
			$this->isvert = $v;
			$this->modifiedColumns[] = Subcategories2Peer::ISVERT;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->id_subcategories1 = $rs->getInt($startcol + 1);

			$this->id_articles = $rs->getInt($startcol + 2);

			$this->name = $rs->getString($startcol + 3);

			$this->extmodule = $rs->getString($startcol + 4);

			$this->parent_id = $rs->getInt($startcol + 5);

			$this->position = $rs->getInt($startcol + 6);

			$this->lang_id = $rs->getString($startcol + 7);

			$this->status = $rs->getInt($startcol + 8);

			$this->isvert = $rs->getInt($startcol + 9);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 10; 
		} catch (Exception $e) {
			throw new PropelException("Error populating Subcategories2 object", $e);
		}
	}

	
	public function delete($con = null)
	{

    foreach (sfMixer::getCallables('BaseSubcategories2:delete:pre') as $callable)
    {
      $ret = call_user_func($callable, $this, $con);
      if ($ret)
      {
        return;
      }
    }


		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(Subcategories2Peer::DATABASE_NAME);
		}

		try {
			$con->begin();
			Subcategories2Peer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	

    foreach (sfMixer::getCallables('BaseSubcategories2:delete:post') as $callable)
    {
      call_user_func($callable, $this, $con);
    }

  }
	
	public function save($con = null)
	{

    foreach (sfMixer::getCallables('BaseSubcategories2:save:pre') as $callable)
    {
      $affectedRows = call_user_func($callable, $this, $con);
      if (is_int($affectedRows))
      {
        return $affectedRows;
      }
    }


		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(Subcategories2Peer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
    foreach (sfMixer::getCallables('BaseSubcategories2:save:post') as $callable)
    {
      call_user_func($callable, $this, $con, $affectedRows);
    }

			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


												
			if ($this->aSubcategories1 !== null) {
				if ($this->aSubcategories1->isModified()) {
					$affectedRows += $this->aSubcategories1->save($con);
				}
				$this->setSubcategories1($this->aSubcategories1);
			}

			if ($this->aArticles !== null) {
				if ($this->aArticles->isModified()) {
					$affectedRows += $this->aArticles->save($con);
				}
				$this->setArticles($this->aArticles);
			}


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = Subcategories2Peer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += Subcategories2Peer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


												
			if ($this->aSubcategories1 !== null) {
				if (!$this->aSubcategories1->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aSubcategories1->getValidationFailures());
				}
			}

			if ($this->aArticles !== null) {
				if (!$this->aArticles->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aArticles->getValidationFailures());
				}
			}


			if (($retval = Subcategories2Peer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = Subcategories2Peer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getIdSubcategories1();
				break;
			case 2:
				return $this->getIdArticles();
				break;
			case 3:
				return $this->getName();
				break;
			case 4:
				return $this->getExtmodule();
				break;
			case 5:
				return $this->getParentId();
				break;
			case 6:
				return $this->getPosition();
				break;
			case 7:
				return $this->getLangId();
				break;
			case 8:
				return $this->getStatus();
				break;
			case 9:
				return $this->getIsvert();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = Subcategories2Peer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getIdSubcategories1(),
			$keys[2] => $this->getIdArticles(),
			$keys[3] => $this->getName(),
			$keys[4] => $this->getExtmodule(),
			$keys[5] => $this->getParentId(),
			$keys[6] => $this->getPosition(),
			$keys[7] => $this->getLangId(),
			$keys[8] => $this->getStatus(),
			$keys[9] => $this->getIsvert(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = Subcategories2Peer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setIdSubcategories1($value);
				break;
			case 2:
				$this->setIdArticles($value);
				break;
			case 3:
				$this->setName($value);
				break;
			case 4:
				$this->setExtmodule($value);
				break;
			case 5:
				$this->setParentId($value);
				break;
			case 6:
				$this->setPosition($value);
				break;
			case 7:
				$this->setLangId($value);
				break;
			case 8:
				$this->setStatus($value);
				break;
			case 9:
				$this->setIsvert($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = Subcategories2Peer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setIdSubcategories1($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setIdArticles($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setName($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setExtmodule($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setParentId($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setPosition($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setLangId($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setStatus($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setIsvert($arr[$keys[9]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(Subcategories2Peer::DATABASE_NAME);

		if ($this->isColumnModified(Subcategories2Peer::ID)) $criteria->add(Subcategories2Peer::ID, $this->id);
		if ($this->isColumnModified(Subcategories2Peer::ID_SUBCATEGORIES1)) $criteria->add(Subcategories2Peer::ID_SUBCATEGORIES1, $this->id_subcategories1);
		if ($this->isColumnModified(Subcategories2Peer::ID_ARTICLES)) $criteria->add(Subcategories2Peer::ID_ARTICLES, $this->id_articles);
		if ($this->isColumnModified(Subcategories2Peer::NAME)) $criteria->add(Subcategories2Peer::NAME, $this->name);
		if ($this->isColumnModified(Subcategories2Peer::EXTMODULE)) $criteria->add(Subcategories2Peer::EXTMODULE, $this->extmodule);
		if ($this->isColumnModified(Subcategories2Peer::PARENT_ID)) $criteria->add(Subcategories2Peer::PARENT_ID, $this->parent_id);
		if ($this->isColumnModified(Subcategories2Peer::POSITION)) $criteria->add(Subcategories2Peer::POSITION, $this->position);
		if ($this->isColumnModified(Subcategories2Peer::LANG_ID)) $criteria->add(Subcategories2Peer::LANG_ID, $this->lang_id);
		if ($this->isColumnModified(Subcategories2Peer::STATUS)) $criteria->add(Subcategories2Peer::STATUS, $this->status);
		if ($this->isColumnModified(Subcategories2Peer::ISVERT)) $criteria->add(Subcategories2Peer::ISVERT, $this->isvert);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(Subcategories2Peer::DATABASE_NAME);

		$criteria->add(Subcategories2Peer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setIdSubcategories1($this->id_subcategories1);

		$copyObj->setIdArticles($this->id_articles);

		$copyObj->setName($this->name);

		$copyObj->setExtmodule($this->extmodule);

		$copyObj->setParentId($this->parent_id);

		$copyObj->setPosition($this->position);

		$copyObj->setLangId($this->lang_id);

		$copyObj->setStatus($this->status);

		$copyObj->setIsvert($this->isvert);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new Subcategories2Peer();
		}
		return self::$peer;
	}

	
	public function setSubcategories1($v)
	{


		if ($v === null) {
			$this->setIdSubcategories1(NULL);
		} else {
			$this->setIdSubcategories1($v->getId());
		}


		$this->aSubcategories1 = $v;
	}


	
	public function getSubcategories1($con = null)
	{
		if ($this->aSubcategories1 === null && ($this->id_subcategories1 !== null)) {
						include_once 'lib/model/om/BaseSubcategories1Peer.php';

			$this->aSubcategories1 = Subcategories1Peer::retrieveByPK($this->id_subcategories1, $con);

			
		}
		return $this->aSubcategories1;
	}

	
	public function setArticles($v)
	{


		if ($v === null) {
			$this->setIdArticles(NULL);
		} else {
			$this->setIdArticles($v->getId());
		}


		$this->aArticles = $v;
	}


	
	public function getArticles($con = null)
	{
		if ($this->aArticles === null && ($this->id_articles !== null)) {
						include_once 'lib/model/om/BaseArticlesPeer.php';

			$this->aArticles = ArticlesPeer::retrieveByPK($this->id_articles, $con);

			
		}
		return $this->aArticles;
	}


  public function __call($method, $arguments)
  {
    if (!$callable = sfMixer::getCallable('BaseSubcategories2:'.$method))
    {
      throw new sfException(sprintf('Call to undefined method BaseSubcategories2::%s', $method));
    }

    array_unshift($arguments, $this);

    return call_user_func_array($callable, $arguments);
  }


} 