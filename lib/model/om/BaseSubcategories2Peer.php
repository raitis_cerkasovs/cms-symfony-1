<?php


abstract class BaseSubcategories2Peer {

	
	const DATABASE_NAME = 'propel';

	
	const TABLE_NAME = 'subcategories2';

	
	const CLASS_DEFAULT = 'lib.model.Subcategories2';

	
	const NUM_COLUMNS = 10;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID = 'subcategories2.ID';

	
	const ID_SUBCATEGORIES1 = 'subcategories2.ID_SUBCATEGORIES1';

	
	const ID_ARTICLES = 'subcategories2.ID_ARTICLES';

	
	const NAME = 'subcategories2.NAME';

	
	const EXTMODULE = 'subcategories2.EXTMODULE';

	
	const PARENT_ID = 'subcategories2.PARENT_ID';

	
	const POSITION = 'subcategories2.POSITION';

	
	const LANG_ID = 'subcategories2.LANG_ID';

	
	const STATUS = 'subcategories2.STATUS';

	
	const ISVERT = 'subcategories2.ISVERT';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Id', 'IdSubcategories1', 'IdArticles', 'Name', 'Extmodule', 'ParentId', 'Position', 'LangId', 'Status', 'Isvert', ),
		BasePeer::TYPE_COLNAME => array (Subcategories2Peer::ID, Subcategories2Peer::ID_SUBCATEGORIES1, Subcategories2Peer::ID_ARTICLES, Subcategories2Peer::NAME, Subcategories2Peer::EXTMODULE, Subcategories2Peer::PARENT_ID, Subcategories2Peer::POSITION, Subcategories2Peer::LANG_ID, Subcategories2Peer::STATUS, Subcategories2Peer::ISVERT, ),
		BasePeer::TYPE_FIELDNAME => array ('id', 'id_subcategories1', 'id_articles', 'name', 'extModule', 'parent_id', 'position', 'lang_id', 'status', 'isVert', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'IdSubcategories1' => 1, 'IdArticles' => 2, 'Name' => 3, 'Extmodule' => 4, 'ParentId' => 5, 'Position' => 6, 'LangId' => 7, 'Status' => 8, 'Isvert' => 9, ),
		BasePeer::TYPE_COLNAME => array (Subcategories2Peer::ID => 0, Subcategories2Peer::ID_SUBCATEGORIES1 => 1, Subcategories2Peer::ID_ARTICLES => 2, Subcategories2Peer::NAME => 3, Subcategories2Peer::EXTMODULE => 4, Subcategories2Peer::PARENT_ID => 5, Subcategories2Peer::POSITION => 6, Subcategories2Peer::LANG_ID => 7, Subcategories2Peer::STATUS => 8, Subcategories2Peer::ISVERT => 9, ),
		BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'id_subcategories1' => 1, 'id_articles' => 2, 'name' => 3, 'extModule' => 4, 'parent_id' => 5, 'position' => 6, 'lang_id' => 7, 'status' => 8, 'isVert' => 9, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/map/Subcategories2MapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.map.Subcategories2MapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = Subcategories2Peer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(Subcategories2Peer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(Subcategories2Peer::ID);

		$criteria->addSelectColumn(Subcategories2Peer::ID_SUBCATEGORIES1);

		$criteria->addSelectColumn(Subcategories2Peer::ID_ARTICLES);

		$criteria->addSelectColumn(Subcategories2Peer::NAME);

		$criteria->addSelectColumn(Subcategories2Peer::EXTMODULE);

		$criteria->addSelectColumn(Subcategories2Peer::PARENT_ID);

		$criteria->addSelectColumn(Subcategories2Peer::POSITION);

		$criteria->addSelectColumn(Subcategories2Peer::LANG_ID);

		$criteria->addSelectColumn(Subcategories2Peer::STATUS);

		$criteria->addSelectColumn(Subcategories2Peer::ISVERT);

	}

	const COUNT = 'COUNT(subcategories2.ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT subcategories2.ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(Subcategories2Peer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(Subcategories2Peer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = Subcategories2Peer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = Subcategories2Peer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return Subcategories2Peer::populateObjects(Subcategories2Peer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{

    foreach (sfMixer::getCallables('BaseSubcategories2Peer:addDoSelectRS:addDoSelectRS') as $callable)
    {
      call_user_func($callable, 'BaseSubcategories2Peer', $criteria, $con);
    }


		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			Subcategories2Peer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = Subcategories2Peer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}

	
	public static function doCountJoinSubcategories1(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(Subcategories2Peer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(Subcategories2Peer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(Subcategories2Peer::ID_SUBCATEGORIES1, Subcategories1Peer::ID);

		$rs = Subcategories2Peer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinArticles(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(Subcategories2Peer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(Subcategories2Peer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(Subcategories2Peer::ID_ARTICLES, ArticlesPeer::ID);

		$rs = Subcategories2Peer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinSubcategories1(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		Subcategories2Peer::addSelectColumns($c);
		$startcol = (Subcategories2Peer::NUM_COLUMNS - Subcategories2Peer::NUM_LAZY_LOAD_COLUMNS) + 1;
		Subcategories1Peer::addSelectColumns($c);

		$c->addJoin(Subcategories2Peer::ID_SUBCATEGORIES1, Subcategories1Peer::ID);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = Subcategories2Peer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = Subcategories1Peer::getOMClass();

			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getSubcategories1(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
										$temp_obj2->addSubcategories2($obj1); 					break;
				}
			}
			if ($newObject) {
				$obj2->initSubcategories2s();
				$obj2->addSubcategories2($obj1); 			}
			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinArticles(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		Subcategories2Peer::addSelectColumns($c);
		$startcol = (Subcategories2Peer::NUM_COLUMNS - Subcategories2Peer::NUM_LAZY_LOAD_COLUMNS) + 1;
		ArticlesPeer::addSelectColumns($c);

		$c->addJoin(Subcategories2Peer::ID_ARTICLES, ArticlesPeer::ID);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = Subcategories2Peer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = ArticlesPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getArticles(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
										$temp_obj2->addSubcategories2($obj1); 					break;
				}
			}
			if ($newObject) {
				$obj2->initSubcategories2s();
				$obj2->addSubcategories2($obj1); 			}
			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doCountJoinAll(Criteria $criteria, $distinct = false, $con = null)
	{
		$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(Subcategories2Peer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(Subcategories2Peer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(Subcategories2Peer::ID_SUBCATEGORIES1, Subcategories1Peer::ID);

		$criteria->addJoin(Subcategories2Peer::ID_ARTICLES, ArticlesPeer::ID);

		$rs = Subcategories2Peer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinAll(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		Subcategories2Peer::addSelectColumns($c);
		$startcol2 = (Subcategories2Peer::NUM_COLUMNS - Subcategories2Peer::NUM_LAZY_LOAD_COLUMNS) + 1;

		Subcategories1Peer::addSelectColumns($c);
		$startcol3 = $startcol2 + Subcategories1Peer::NUM_COLUMNS;

		ArticlesPeer::addSelectColumns($c);
		$startcol4 = $startcol3 + ArticlesPeer::NUM_COLUMNS;

		$c->addJoin(Subcategories2Peer::ID_SUBCATEGORIES1, Subcategories1Peer::ID);

		$c->addJoin(Subcategories2Peer::ID_ARTICLES, ArticlesPeer::ID);

		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = Subcategories2Peer::getOMClass();


			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);


					
			$omClass = Subcategories1Peer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getSubcategories1(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addSubcategories2($obj1); 					break;
				}
			}

			if ($newObject) {
				$obj2->initSubcategories2s();
				$obj2->addSubcategories2($obj1);
			}


					
			$omClass = ArticlesPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj3 = new $cls();
			$obj3->hydrate($rs, $startcol3);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj3 = $temp_obj1->getArticles(); 				if ($temp_obj3->getPrimaryKey() === $obj3->getPrimaryKey()) {
					$newObject = false;
					$temp_obj3->addSubcategories2($obj1); 					break;
				}
			}

			if ($newObject) {
				$obj3->initSubcategories2s();
				$obj3->addSubcategories2($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doCountJoinAllExceptSubcategories1(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(Subcategories2Peer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(Subcategories2Peer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(Subcategories2Peer::ID_ARTICLES, ArticlesPeer::ID);

		$rs = Subcategories2Peer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinAllExceptArticles(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(Subcategories2Peer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(Subcategories2Peer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(Subcategories2Peer::ID_SUBCATEGORIES1, Subcategories1Peer::ID);

		$rs = Subcategories2Peer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinAllExceptSubcategories1(Criteria $c, $con = null)
	{
		$c = clone $c;

								if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		Subcategories2Peer::addSelectColumns($c);
		$startcol2 = (Subcategories2Peer::NUM_COLUMNS - Subcategories2Peer::NUM_LAZY_LOAD_COLUMNS) + 1;

		ArticlesPeer::addSelectColumns($c);
		$startcol3 = $startcol2 + ArticlesPeer::NUM_COLUMNS;

		$c->addJoin(Subcategories2Peer::ID_ARTICLES, ArticlesPeer::ID);


		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = Subcategories2Peer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = ArticlesPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2  = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getArticles(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addSubcategories2($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj2->initSubcategories2s();
				$obj2->addSubcategories2($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinAllExceptArticles(Criteria $c, $con = null)
	{
		$c = clone $c;

								if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		Subcategories2Peer::addSelectColumns($c);
		$startcol2 = (Subcategories2Peer::NUM_COLUMNS - Subcategories2Peer::NUM_LAZY_LOAD_COLUMNS) + 1;

		Subcategories1Peer::addSelectColumns($c);
		$startcol3 = $startcol2 + Subcategories1Peer::NUM_COLUMNS;

		$c->addJoin(Subcategories2Peer::ID_SUBCATEGORIES1, Subcategories1Peer::ID);


		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = Subcategories2Peer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = Subcategories1Peer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2  = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getSubcategories1(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addSubcategories2($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj2->initSubcategories2s();
				$obj2->addSubcategories2($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}

	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return Subcategories2Peer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{

    foreach (sfMixer::getCallables('BaseSubcategories2Peer:doInsert:pre') as $callable)
    {
      $ret = call_user_func($callable, 'BaseSubcategories2Peer', $values, $con);
      if (false !== $ret)
      {
        return $ret;
      }
    }


		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(Subcategories2Peer::ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		
    foreach (sfMixer::getCallables('BaseSubcategories2Peer:doInsert:post') as $callable)
    {
      call_user_func($callable, 'BaseSubcategories2Peer', $values, $con, $pk);
    }

    return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{

    foreach (sfMixer::getCallables('BaseSubcategories2Peer:doUpdate:pre') as $callable)
    {
      $ret = call_user_func($callable, 'BaseSubcategories2Peer', $values, $con);
      if (false !== $ret)
      {
        return $ret;
      }
    }


		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(Subcategories2Peer::ID);
			$selectCriteria->add(Subcategories2Peer::ID, $criteria->remove(Subcategories2Peer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		$ret = BasePeer::doUpdate($selectCriteria, $criteria, $con);
	

    foreach (sfMixer::getCallables('BaseSubcategories2Peer:doUpdate:post') as $callable)
    {
      call_user_func($callable, 'BaseSubcategories2Peer', $values, $con, $ret);
    }

    return $ret;
  }

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(Subcategories2Peer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(Subcategories2Peer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof Subcategories2) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(Subcategories2Peer::ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(Subcategories2 $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(Subcategories2Peer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(Subcategories2Peer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(Subcategories2Peer::DATABASE_NAME, Subcategories2Peer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = Subcategories2Peer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(Subcategories2Peer::DATABASE_NAME);

		$criteria->add(Subcategories2Peer::ID, $pk);


		$v = Subcategories2Peer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(Subcategories2Peer::ID, $pks, Criteria::IN);
			$objs = Subcategories2Peer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseSubcategories2Peer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/map/Subcategories2MapBuilder.php';
	Propel::registerMapBuilder('lib.model.map.Subcategories2MapBuilder');
}
