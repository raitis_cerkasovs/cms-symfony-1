<?php


abstract class BaseSubgaleries1 extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $id_galeries;


	
	protected $name;


	
	protected $position;


	
	protected $created;


	
	protected $temp1;


	
	protected $temp2;


	
	protected $temp3;


	
	protected $temp4;

	
	protected $aGaleries;

	
	protected $collFotos;

	
	protected $lastFotoCriteria = null;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getIdGaleries()
	{

		return $this->id_galeries;
	}

	
	public function getName()
	{

		return $this->name;
	}

	
	public function getPosition()
	{

		return $this->position;
	}

	
	public function getCreated($format = 'Y-m-d H:i:s')
	{

		if ($this->created === null || $this->created === '') {
			return null;
		} elseif (!is_int($this->created)) {
						$ts = strtotime($this->created);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [created] as date/time value: " . var_export($this->created, true));
			}
		} else {
			$ts = $this->created;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getTemp1()
	{

		return $this->temp1;
	}

	
	public function getTemp2()
	{

		return $this->temp2;
	}

	
	public function getTemp3()
	{

		return $this->temp3;
	}

	
	public function getTemp4()
	{

		return $this->temp4;
	}

	
	public function setId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = Subgaleries1Peer::ID;
		}

	} 
	
	public function setIdGaleries($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_galeries !== $v) {
			$this->id_galeries = $v;
			$this->modifiedColumns[] = Subgaleries1Peer::ID_GALERIES;
		}

		if ($this->aGaleries !== null && $this->aGaleries->getId() !== $v) {
			$this->aGaleries = null;
		}

	} 
	
	public function setName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->name !== $v) {
			$this->name = $v;
			$this->modifiedColumns[] = Subgaleries1Peer::NAME;
		}

	} 
	
	public function setPosition($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->position !== $v) {
			$this->position = $v;
			$this->modifiedColumns[] = Subgaleries1Peer::POSITION;
		}

	} 
	
	public function setCreated($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [created] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->created !== $ts) {
			$this->created = $ts;
			$this->modifiedColumns[] = Subgaleries1Peer::CREATED;
		}

	} 
	
	public function setTemp1($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->temp1 !== $v) {
			$this->temp1 = $v;
			$this->modifiedColumns[] = Subgaleries1Peer::TEMP1;
		}

	} 
	
	public function setTemp2($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->temp2 !== $v) {
			$this->temp2 = $v;
			$this->modifiedColumns[] = Subgaleries1Peer::TEMP2;
		}

	} 
	
	public function setTemp3($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->temp3 !== $v) {
			$this->temp3 = $v;
			$this->modifiedColumns[] = Subgaleries1Peer::TEMP3;
		}

	} 
	
	public function setTemp4($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->temp4 !== $v) {
			$this->temp4 = $v;
			$this->modifiedColumns[] = Subgaleries1Peer::TEMP4;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->id_galeries = $rs->getInt($startcol + 1);

			$this->name = $rs->getString($startcol + 2);

			$this->position = $rs->getInt($startcol + 3);

			$this->created = $rs->getTimestamp($startcol + 4, null);

			$this->temp1 = $rs->getString($startcol + 5);

			$this->temp2 = $rs->getString($startcol + 6);

			$this->temp3 = $rs->getString($startcol + 7);

			$this->temp4 = $rs->getString($startcol + 8);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 9; 
		} catch (Exception $e) {
			throw new PropelException("Error populating Subgaleries1 object", $e);
		}
	}

	
	public function delete($con = null)
	{

    foreach (sfMixer::getCallables('BaseSubgaleries1:delete:pre') as $callable)
    {
      $ret = call_user_func($callable, $this, $con);
      if ($ret)
      {
        return;
      }
    }


		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(Subgaleries1Peer::DATABASE_NAME);
		}

		try {
			$con->begin();
			Subgaleries1Peer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	

    foreach (sfMixer::getCallables('BaseSubgaleries1:delete:post') as $callable)
    {
      call_user_func($callable, $this, $con);
    }

  }
	
	public function save($con = null)
	{

    foreach (sfMixer::getCallables('BaseSubgaleries1:save:pre') as $callable)
    {
      $affectedRows = call_user_func($callable, $this, $con);
      if (is_int($affectedRows))
      {
        return $affectedRows;
      }
    }


		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(Subgaleries1Peer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
    foreach (sfMixer::getCallables('BaseSubgaleries1:save:post') as $callable)
    {
      call_user_func($callable, $this, $con, $affectedRows);
    }

			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


												
			if ($this->aGaleries !== null) {
				if ($this->aGaleries->isModified()) {
					$affectedRows += $this->aGaleries->save($con);
				}
				$this->setGaleries($this->aGaleries);
			}


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = Subgaleries1Peer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += Subgaleries1Peer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			if ($this->collFotos !== null) {
				foreach($this->collFotos as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


												
			if ($this->aGaleries !== null) {
				if (!$this->aGaleries->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aGaleries->getValidationFailures());
				}
			}


			if (($retval = Subgaleries1Peer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collFotos !== null) {
					foreach($this->collFotos as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = Subgaleries1Peer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getIdGaleries();
				break;
			case 2:
				return $this->getName();
				break;
			case 3:
				return $this->getPosition();
				break;
			case 4:
				return $this->getCreated();
				break;
			case 5:
				return $this->getTemp1();
				break;
			case 6:
				return $this->getTemp2();
				break;
			case 7:
				return $this->getTemp3();
				break;
			case 8:
				return $this->getTemp4();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = Subgaleries1Peer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getIdGaleries(),
			$keys[2] => $this->getName(),
			$keys[3] => $this->getPosition(),
			$keys[4] => $this->getCreated(),
			$keys[5] => $this->getTemp1(),
			$keys[6] => $this->getTemp2(),
			$keys[7] => $this->getTemp3(),
			$keys[8] => $this->getTemp4(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = Subgaleries1Peer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setIdGaleries($value);
				break;
			case 2:
				$this->setName($value);
				break;
			case 3:
				$this->setPosition($value);
				break;
			case 4:
				$this->setCreated($value);
				break;
			case 5:
				$this->setTemp1($value);
				break;
			case 6:
				$this->setTemp2($value);
				break;
			case 7:
				$this->setTemp3($value);
				break;
			case 8:
				$this->setTemp4($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = Subgaleries1Peer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setIdGaleries($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setName($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setPosition($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setCreated($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setTemp1($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setTemp2($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setTemp3($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setTemp4($arr[$keys[8]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(Subgaleries1Peer::DATABASE_NAME);

		if ($this->isColumnModified(Subgaleries1Peer::ID)) $criteria->add(Subgaleries1Peer::ID, $this->id);
		if ($this->isColumnModified(Subgaleries1Peer::ID_GALERIES)) $criteria->add(Subgaleries1Peer::ID_GALERIES, $this->id_galeries);
		if ($this->isColumnModified(Subgaleries1Peer::NAME)) $criteria->add(Subgaleries1Peer::NAME, $this->name);
		if ($this->isColumnModified(Subgaleries1Peer::POSITION)) $criteria->add(Subgaleries1Peer::POSITION, $this->position);
		if ($this->isColumnModified(Subgaleries1Peer::CREATED)) $criteria->add(Subgaleries1Peer::CREATED, $this->created);
		if ($this->isColumnModified(Subgaleries1Peer::TEMP1)) $criteria->add(Subgaleries1Peer::TEMP1, $this->temp1);
		if ($this->isColumnModified(Subgaleries1Peer::TEMP2)) $criteria->add(Subgaleries1Peer::TEMP2, $this->temp2);
		if ($this->isColumnModified(Subgaleries1Peer::TEMP3)) $criteria->add(Subgaleries1Peer::TEMP3, $this->temp3);
		if ($this->isColumnModified(Subgaleries1Peer::TEMP4)) $criteria->add(Subgaleries1Peer::TEMP4, $this->temp4);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(Subgaleries1Peer::DATABASE_NAME);

		$criteria->add(Subgaleries1Peer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setIdGaleries($this->id_galeries);

		$copyObj->setName($this->name);

		$copyObj->setPosition($this->position);

		$copyObj->setCreated($this->created);

		$copyObj->setTemp1($this->temp1);

		$copyObj->setTemp2($this->temp2);

		$copyObj->setTemp3($this->temp3);

		$copyObj->setTemp4($this->temp4);


		if ($deepCopy) {
									$copyObj->setNew(false);

			foreach($this->getFotos() as $relObj) {
				$copyObj->addFoto($relObj->copy($deepCopy));
			}

		} 

		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new Subgaleries1Peer();
		}
		return self::$peer;
	}

	
	public function setGaleries($v)
	{


		if ($v === null) {
			$this->setIdGaleries(NULL);
		} else {
			$this->setIdGaleries($v->getId());
		}


		$this->aGaleries = $v;
	}


	
	public function getGaleries($con = null)
	{
		if ($this->aGaleries === null && ($this->id_galeries !== null)) {
						include_once 'lib/model/om/BaseGaleriesPeer.php';

			$this->aGaleries = GaleriesPeer::retrieveByPK($this->id_galeries, $con);

			
		}
		return $this->aGaleries;
	}

	
	public function initFotos()
	{
		if ($this->collFotos === null) {
			$this->collFotos = array();
		}
	}

	
	public function getFotos($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseFotoPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collFotos === null) {
			if ($this->isNew()) {
			   $this->collFotos = array();
			} else {

				$criteria->add(FotoPeer::ID_SUBGALERIES1, $this->getId());

				FotoPeer::addSelectColumns($criteria);
				$this->collFotos = FotoPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(FotoPeer::ID_SUBGALERIES1, $this->getId());

				FotoPeer::addSelectColumns($criteria);
				if (!isset($this->lastFotoCriteria) || !$this->lastFotoCriteria->equals($criteria)) {
					$this->collFotos = FotoPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastFotoCriteria = $criteria;
		return $this->collFotos;
	}

	
	public function countFotos($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseFotoPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(FotoPeer::ID_SUBGALERIES1, $this->getId());

		return FotoPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addFoto(Foto $l)
	{
		$this->collFotos[] = $l;
		$l->setSubgaleries1($this);
	}


	
	public function getFotosJoinSpecies($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseFotoPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collFotos === null) {
			if ($this->isNew()) {
				$this->collFotos = array();
			} else {

				$criteria->add(FotoPeer::ID_SUBGALERIES1, $this->getId());

				$this->collFotos = FotoPeer::doSelectJoinSpecies($criteria, $con);
			}
		} else {
									
			$criteria->add(FotoPeer::ID_SUBGALERIES1, $this->getId());

			if (!isset($this->lastFotoCriteria) || !$this->lastFotoCriteria->equals($criteria)) {
				$this->collFotos = FotoPeer::doSelectJoinSpecies($criteria, $con);
			}
		}
		$this->lastFotoCriteria = $criteria;

		return $this->collFotos;
	}


	
	public function getFotosJoinGaleries($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseFotoPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collFotos === null) {
			if ($this->isNew()) {
				$this->collFotos = array();
			} else {

				$criteria->add(FotoPeer::ID_SUBGALERIES1, $this->getId());

				$this->collFotos = FotoPeer::doSelectJoinGaleries($criteria, $con);
			}
		} else {
									
			$criteria->add(FotoPeer::ID_SUBGALERIES1, $this->getId());

			if (!isset($this->lastFotoCriteria) || !$this->lastFotoCriteria->equals($criteria)) {
				$this->collFotos = FotoPeer::doSelectJoinGaleries($criteria, $con);
			}
		}
		$this->lastFotoCriteria = $criteria;

		return $this->collFotos;
	}


  public function __call($method, $arguments)
  {
    if (!$callable = sfMixer::getCallable('BaseSubgaleries1:'.$method))
    {
      throw new sfException(sprintf('Call to undefined method BaseSubgaleries1::%s', $method));
    }

    array_unshift($arguments, $this);

    return call_user_func_array($callable, $arguments);
  }


} 