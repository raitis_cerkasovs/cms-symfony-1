<?php


abstract class BaseSubgaleries1Peer {

	
	const DATABASE_NAME = 'propel';

	
	const TABLE_NAME = 'subgaleries1';

	
	const CLASS_DEFAULT = 'lib.model.Subgaleries1';

	
	const NUM_COLUMNS = 9;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID = 'subgaleries1.ID';

	
	const ID_GALERIES = 'subgaleries1.ID_GALERIES';

	
	const NAME = 'subgaleries1.NAME';

	
	const POSITION = 'subgaleries1.POSITION';

	
	const CREATED = 'subgaleries1.CREATED';

	
	const TEMP1 = 'subgaleries1.TEMP1';

	
	const TEMP2 = 'subgaleries1.TEMP2';

	
	const TEMP3 = 'subgaleries1.TEMP3';

	
	const TEMP4 = 'subgaleries1.TEMP4';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Id', 'IdGaleries', 'Name', 'Position', 'Created', 'Temp1', 'Temp2', 'Temp3', 'Temp4', ),
		BasePeer::TYPE_COLNAME => array (Subgaleries1Peer::ID, Subgaleries1Peer::ID_GALERIES, Subgaleries1Peer::NAME, Subgaleries1Peer::POSITION, Subgaleries1Peer::CREATED, Subgaleries1Peer::TEMP1, Subgaleries1Peer::TEMP2, Subgaleries1Peer::TEMP3, Subgaleries1Peer::TEMP4, ),
		BasePeer::TYPE_FIELDNAME => array ('id', 'id_galeries', 'name', 'position', 'created', 'temp1', 'temp2', 'temp3', 'temp4', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'IdGaleries' => 1, 'Name' => 2, 'Position' => 3, 'Created' => 4, 'Temp1' => 5, 'Temp2' => 6, 'Temp3' => 7, 'Temp4' => 8, ),
		BasePeer::TYPE_COLNAME => array (Subgaleries1Peer::ID => 0, Subgaleries1Peer::ID_GALERIES => 1, Subgaleries1Peer::NAME => 2, Subgaleries1Peer::POSITION => 3, Subgaleries1Peer::CREATED => 4, Subgaleries1Peer::TEMP1 => 5, Subgaleries1Peer::TEMP2 => 6, Subgaleries1Peer::TEMP3 => 7, Subgaleries1Peer::TEMP4 => 8, ),
		BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'id_galeries' => 1, 'name' => 2, 'position' => 3, 'created' => 4, 'temp1' => 5, 'temp2' => 6, 'temp3' => 7, 'temp4' => 8, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/map/Subgaleries1MapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.map.Subgaleries1MapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = Subgaleries1Peer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(Subgaleries1Peer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(Subgaleries1Peer::ID);

		$criteria->addSelectColumn(Subgaleries1Peer::ID_GALERIES);

		$criteria->addSelectColumn(Subgaleries1Peer::NAME);

		$criteria->addSelectColumn(Subgaleries1Peer::POSITION);

		$criteria->addSelectColumn(Subgaleries1Peer::CREATED);

		$criteria->addSelectColumn(Subgaleries1Peer::TEMP1);

		$criteria->addSelectColumn(Subgaleries1Peer::TEMP2);

		$criteria->addSelectColumn(Subgaleries1Peer::TEMP3);

		$criteria->addSelectColumn(Subgaleries1Peer::TEMP4);

	}

	const COUNT = 'COUNT(subgaleries1.ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT subgaleries1.ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(Subgaleries1Peer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(Subgaleries1Peer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = Subgaleries1Peer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = Subgaleries1Peer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return Subgaleries1Peer::populateObjects(Subgaleries1Peer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{

    foreach (sfMixer::getCallables('BaseSubgaleries1Peer:addDoSelectRS:addDoSelectRS') as $callable)
    {
      call_user_func($callable, 'BaseSubgaleries1Peer', $criteria, $con);
    }


		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			Subgaleries1Peer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = Subgaleries1Peer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}

	
	public static function doCountJoinGaleries(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(Subgaleries1Peer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(Subgaleries1Peer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(Subgaleries1Peer::ID_GALERIES, GaleriesPeer::ID);

		$rs = Subgaleries1Peer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinGaleries(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		Subgaleries1Peer::addSelectColumns($c);
		$startcol = (Subgaleries1Peer::NUM_COLUMNS - Subgaleries1Peer::NUM_LAZY_LOAD_COLUMNS) + 1;
		GaleriesPeer::addSelectColumns($c);

		$c->addJoin(Subgaleries1Peer::ID_GALERIES, GaleriesPeer::ID);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = Subgaleries1Peer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = GaleriesPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getGaleries(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
										$temp_obj2->addSubgaleries1($obj1); 					break;
				}
			}
			if ($newObject) {
				$obj2->initSubgaleries1s();
				$obj2->addSubgaleries1($obj1); 			}
			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doCountJoinAll(Criteria $criteria, $distinct = false, $con = null)
	{
		$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(Subgaleries1Peer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(Subgaleries1Peer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(Subgaleries1Peer::ID_GALERIES, GaleriesPeer::ID);

		$rs = Subgaleries1Peer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinAll(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		Subgaleries1Peer::addSelectColumns($c);
		$startcol2 = (Subgaleries1Peer::NUM_COLUMNS - Subgaleries1Peer::NUM_LAZY_LOAD_COLUMNS) + 1;

		GaleriesPeer::addSelectColumns($c);
		$startcol3 = $startcol2 + GaleriesPeer::NUM_COLUMNS;

		$c->addJoin(Subgaleries1Peer::ID_GALERIES, GaleriesPeer::ID);

		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = Subgaleries1Peer::getOMClass();


			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);


					
			$omClass = GaleriesPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getGaleries(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addSubgaleries1($obj1); 					break;
				}
			}

			if ($newObject) {
				$obj2->initSubgaleries1s();
				$obj2->addSubgaleries1($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}

	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return Subgaleries1Peer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{

    foreach (sfMixer::getCallables('BaseSubgaleries1Peer:doInsert:pre') as $callable)
    {
      $ret = call_user_func($callable, 'BaseSubgaleries1Peer', $values, $con);
      if (false !== $ret)
      {
        return $ret;
      }
    }


		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(Subgaleries1Peer::ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		
    foreach (sfMixer::getCallables('BaseSubgaleries1Peer:doInsert:post') as $callable)
    {
      call_user_func($callable, 'BaseSubgaleries1Peer', $values, $con, $pk);
    }

    return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{

    foreach (sfMixer::getCallables('BaseSubgaleries1Peer:doUpdate:pre') as $callable)
    {
      $ret = call_user_func($callable, 'BaseSubgaleries1Peer', $values, $con);
      if (false !== $ret)
      {
        return $ret;
      }
    }


		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(Subgaleries1Peer::ID);
			$selectCriteria->add(Subgaleries1Peer::ID, $criteria->remove(Subgaleries1Peer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		$ret = BasePeer::doUpdate($selectCriteria, $criteria, $con);
	

    foreach (sfMixer::getCallables('BaseSubgaleries1Peer:doUpdate:post') as $callable)
    {
      call_user_func($callable, 'BaseSubgaleries1Peer', $values, $con, $ret);
    }

    return $ret;
  }

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(Subgaleries1Peer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(Subgaleries1Peer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof Subgaleries1) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(Subgaleries1Peer::ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(Subgaleries1 $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(Subgaleries1Peer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(Subgaleries1Peer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(Subgaleries1Peer::DATABASE_NAME, Subgaleries1Peer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = Subgaleries1Peer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(Subgaleries1Peer::DATABASE_NAME);

		$criteria->add(Subgaleries1Peer::ID, $pk);


		$v = Subgaleries1Peer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(Subgaleries1Peer::ID, $pks, Criteria::IN);
			$objs = Subgaleries1Peer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseSubgaleries1Peer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/map/Subgaleries1MapBuilder.php';
	Propel::registerMapBuilder('lib.model.map.Subgaleries1MapBuilder');
}
