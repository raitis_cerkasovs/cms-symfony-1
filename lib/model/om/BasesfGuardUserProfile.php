<?php


abstract class BasesfGuardUserProfile extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $user_id;


	
	protected $email;


	
	protected $fullname;


	
	protected $validate;


	
	protected $temp1;


	
	protected $temp2;


	
	protected $temp3;


	
	protected $id;

	
	protected $asfGuardUser;

	
	protected $collSluds;

	
	protected $lastSludCriteria = null;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getUserId()
	{

		return $this->user_id;
	}

	
	public function getEmail()
	{

		return $this->email;
	}

	
	public function getFullname()
	{

		return $this->fullname;
	}

	
	public function getValidate()
	{

		return $this->validate;
	}

	
	public function getTemp1()
	{

		return $this->temp1;
	}

	
	public function getTemp2()
	{

		return $this->temp2;
	}

	
	public function getTemp3()
	{

		return $this->temp3;
	}

	
	public function getId()
	{

		return $this->id;
	}

	
	public function setUserId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->user_id !== $v) {
			$this->user_id = $v;
			$this->modifiedColumns[] = sfGuardUserProfilePeer::USER_ID;
		}

		if ($this->asfGuardUser !== null && $this->asfGuardUser->getId() !== $v) {
			$this->asfGuardUser = null;
		}

	} 
	
	public function setEmail($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->email !== $v) {
			$this->email = $v;
			$this->modifiedColumns[] = sfGuardUserProfilePeer::EMAIL;
		}

	} 
	
	public function setFullname($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->fullname !== $v) {
			$this->fullname = $v;
			$this->modifiedColumns[] = sfGuardUserProfilePeer::FULLNAME;
		}

	} 
	
	public function setValidate($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->validate !== $v) {
			$this->validate = $v;
			$this->modifiedColumns[] = sfGuardUserProfilePeer::VALIDATE;
		}

	} 
	
	public function setTemp1($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->temp1 !== $v) {
			$this->temp1 = $v;
			$this->modifiedColumns[] = sfGuardUserProfilePeer::TEMP1;
		}

	} 
	
	public function setTemp2($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->temp2 !== $v) {
			$this->temp2 = $v;
			$this->modifiedColumns[] = sfGuardUserProfilePeer::TEMP2;
		}

	} 
	
	public function setTemp3($v)
	{

		
		
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->temp3 !== $v) {
			$this->temp3 = $v;
			$this->modifiedColumns[] = sfGuardUserProfilePeer::TEMP3;
		}

	} 
	
	public function setId($v)
	{

		
		
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = sfGuardUserProfilePeer::ID;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->user_id = $rs->getInt($startcol + 0);

			$this->email = $rs->getString($startcol + 1);

			$this->fullname = $rs->getString($startcol + 2);

			$this->validate = $rs->getString($startcol + 3);

			$this->temp1 = $rs->getString($startcol + 4);

			$this->temp2 = $rs->getString($startcol + 5);

			$this->temp3 = $rs->getString($startcol + 6);

			$this->id = $rs->getInt($startcol + 7);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 8; 
		} catch (Exception $e) {
			throw new PropelException("Error populating sfGuardUserProfile object", $e);
		}
	}

	
	public function delete($con = null)
	{

    foreach (sfMixer::getCallables('BasesfGuardUserProfile:delete:pre') as $callable)
    {
      $ret = call_user_func($callable, $this, $con);
      if ($ret)
      {
        return;
      }
    }


		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(sfGuardUserProfilePeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			sfGuardUserProfilePeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	

    foreach (sfMixer::getCallables('BasesfGuardUserProfile:delete:post') as $callable)
    {
      call_user_func($callable, $this, $con);
    }

  }
	
	public function save($con = null)
	{

    foreach (sfMixer::getCallables('BasesfGuardUserProfile:save:pre') as $callable)
    {
      $affectedRows = call_user_func($callable, $this, $con);
      if (is_int($affectedRows))
      {
        return $affectedRows;
      }
    }


		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(sfGuardUserProfilePeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
    foreach (sfMixer::getCallables('BasesfGuardUserProfile:save:post') as $callable)
    {
      call_user_func($callable, $this, $con, $affectedRows);
    }

			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


												
			if ($this->asfGuardUser !== null) {
				if ($this->asfGuardUser->isModified()) {
					$affectedRows += $this->asfGuardUser->save($con);
				}
				$this->setsfGuardUser($this->asfGuardUser);
			}


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = sfGuardUserProfilePeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += sfGuardUserProfilePeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			if ($this->collSluds !== null) {
				foreach($this->collSluds as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


												
			if ($this->asfGuardUser !== null) {
				if (!$this->asfGuardUser->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->asfGuardUser->getValidationFailures());
				}
			}


			if (($retval = sfGuardUserProfilePeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collSluds !== null) {
					foreach($this->collSluds as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = sfGuardUserProfilePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getUserId();
				break;
			case 1:
				return $this->getEmail();
				break;
			case 2:
				return $this->getFullname();
				break;
			case 3:
				return $this->getValidate();
				break;
			case 4:
				return $this->getTemp1();
				break;
			case 5:
				return $this->getTemp2();
				break;
			case 6:
				return $this->getTemp3();
				break;
			case 7:
				return $this->getId();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = sfGuardUserProfilePeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getUserId(),
			$keys[1] => $this->getEmail(),
			$keys[2] => $this->getFullname(),
			$keys[3] => $this->getValidate(),
			$keys[4] => $this->getTemp1(),
			$keys[5] => $this->getTemp2(),
			$keys[6] => $this->getTemp3(),
			$keys[7] => $this->getId(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = sfGuardUserProfilePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setUserId($value);
				break;
			case 1:
				$this->setEmail($value);
				break;
			case 2:
				$this->setFullname($value);
				break;
			case 3:
				$this->setValidate($value);
				break;
			case 4:
				$this->setTemp1($value);
				break;
			case 5:
				$this->setTemp2($value);
				break;
			case 6:
				$this->setTemp3($value);
				break;
			case 7:
				$this->setId($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = sfGuardUserProfilePeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setUserId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setEmail($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setFullname($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setValidate($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setTemp1($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setTemp2($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setTemp3($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setId($arr[$keys[7]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(sfGuardUserProfilePeer::DATABASE_NAME);

		if ($this->isColumnModified(sfGuardUserProfilePeer::USER_ID)) $criteria->add(sfGuardUserProfilePeer::USER_ID, $this->user_id);
		if ($this->isColumnModified(sfGuardUserProfilePeer::EMAIL)) $criteria->add(sfGuardUserProfilePeer::EMAIL, $this->email);
		if ($this->isColumnModified(sfGuardUserProfilePeer::FULLNAME)) $criteria->add(sfGuardUserProfilePeer::FULLNAME, $this->fullname);
		if ($this->isColumnModified(sfGuardUserProfilePeer::VALIDATE)) $criteria->add(sfGuardUserProfilePeer::VALIDATE, $this->validate);
		if ($this->isColumnModified(sfGuardUserProfilePeer::TEMP1)) $criteria->add(sfGuardUserProfilePeer::TEMP1, $this->temp1);
		if ($this->isColumnModified(sfGuardUserProfilePeer::TEMP2)) $criteria->add(sfGuardUserProfilePeer::TEMP2, $this->temp2);
		if ($this->isColumnModified(sfGuardUserProfilePeer::TEMP3)) $criteria->add(sfGuardUserProfilePeer::TEMP3, $this->temp3);
		if ($this->isColumnModified(sfGuardUserProfilePeer::ID)) $criteria->add(sfGuardUserProfilePeer::ID, $this->id);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(sfGuardUserProfilePeer::DATABASE_NAME);

		$criteria->add(sfGuardUserProfilePeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setUserId($this->user_id);

		$copyObj->setEmail($this->email);

		$copyObj->setFullname($this->fullname);

		$copyObj->setValidate($this->validate);

		$copyObj->setTemp1($this->temp1);

		$copyObj->setTemp2($this->temp2);

		$copyObj->setTemp3($this->temp3);


		if ($deepCopy) {
									$copyObj->setNew(false);

			foreach($this->getSluds() as $relObj) {
				$copyObj->addSlud($relObj->copy($deepCopy));
			}

		} 

		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new sfGuardUserProfilePeer();
		}
		return self::$peer;
	}

	
	public function setsfGuardUser($v)
	{


		if ($v === null) {
			$this->setUserId(NULL);
		} else {
			$this->setUserId($v->getId());
		}


		$this->asfGuardUser = $v;
	}


	
	public function getsfGuardUser($con = null)
	{
		if ($this->asfGuardUser === null && ($this->user_id !== null)) {
						include_once 'plugins/sfGuardPlugin/lib/model/om/BasesfGuardUserPeer.php';

			$this->asfGuardUser = sfGuardUserPeer::retrieveByPK($this->user_id, $con);

			
		}
		return $this->asfGuardUser;
	}

	
	public function initSluds()
	{
		if ($this->collSluds === null) {
			$this->collSluds = array();
		}
	}

	
	public function getSluds($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseSludPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collSluds === null) {
			if ($this->isNew()) {
			   $this->collSluds = array();
			} else {

				$criteria->add(SludPeer::IDUSER, $this->getUserId());

				SludPeer::addSelectColumns($criteria);
				$this->collSluds = SludPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(SludPeer::IDUSER, $this->getUserId());

				SludPeer::addSelectColumns($criteria);
				if (!isset($this->lastSludCriteria) || !$this->lastSludCriteria->equals($criteria)) {
					$this->collSluds = SludPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastSludCriteria = $criteria;
		return $this->collSluds;
	}

	
	public function countSluds($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseSludPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(SludPeer::IDUSER, $this->getUserId());

		return SludPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addSlud(Slud $l)
	{
		$this->collSluds[] = $l;
		$l->setsfGuardUserProfile($this);
	}


	
	public function getSludsJoinSludtips($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseSludPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collSluds === null) {
			if ($this->isNew()) {
				$this->collSluds = array();
			} else {

				$criteria->add(SludPeer::IDUSER, $this->getUserId());

				$this->collSluds = SludPeer::doSelectJoinSludtips($criteria, $con);
			}
		} else {
									
			$criteria->add(SludPeer::IDUSER, $this->getUserId());

			if (!isset($this->lastSludCriteria) || !$this->lastSludCriteria->equals($criteria)) {
				$this->collSluds = SludPeer::doSelectJoinSludtips($criteria, $con);
			}
		}
		$this->lastSludCriteria = $criteria;

		return $this->collSluds;
	}


  public function __call($method, $arguments)
  {
    if (!$callable = sfMixer::getCallable('BasesfGuardUserProfile:'.$method))
    {
      throw new sfException(sprintf('Call to undefined method BasesfGuardUserProfile::%s', $method));
    }

    array_unshift($arguments, $this);

    return call_user_func_array($callable, $arguments);
  }


} 