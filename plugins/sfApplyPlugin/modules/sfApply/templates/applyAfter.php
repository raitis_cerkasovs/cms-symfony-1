﻿<?php use_helper('I18N') ?>
<div class="sf_apply_notice">
<p>
<?php if ($sf_user->getCulture() == 'lv_LV'):?>
Paldies par reģistrēšanos mūsu portālā! Pēc brīža Jūs saņemsiet apstiprinājumu savā elektroniskajā pastkastē. Ja šādu ziņu nevarat atrast, pārbaudiet sadaļas „spam” vai „bulk”.
<?php else:?>
Спасибо за регистрацию на нашем портале! Уже сейчас Вы получите подтверждение на свой электронный ящик. Если Вы не сможете найти такое сообщение, проверьте разделы "Spam" или "Bulk".
<?php endif; ?>
</p>
<br />
<p>
<?php if ($sf_user->getCulture() == 'lv_LV'):?>
<?php echo button_to(__("Turpināt"), "articles/show?id=1") ?>
<?php else:?>
<?php echo button_to(__("Turpināt"), "articles/show?id=21") ?>
<?php endif;?>
</p>
</div>
