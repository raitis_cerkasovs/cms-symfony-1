﻿<div class="sf_apply_notice">
<?php if ($sf_user->getCulture() == 'lv_LV'):?>
Apstiprinājuma kods nav derīgs.<br /><br />
Tas var būt tāpēc, ka savu reģistrāciju jau esat apstiprinājis iepriekš. Ja tā ir, tad vienkārši ieejiet sistēmā.<br />
Citi iespējamie iemesli:<br /><br />
1.	Ja iekopējāt URL no reģistrācijas e-pasta, lūdzu, pārliecinieties, ka izdarījāt to pilnībā un bez kļūdām.<br />
2.	Ja saņēmāt reģistrāciju apstiprinošo e-pastu pirms ilgāka laika un šai laikā neapstiprinājāt savu reģistrēšanos, iespējams, ka Jūsu reģistrācija vairs nav aktīva. Šādā gadījumā Jums jāreģistrējas no jauna.
<?php else:?>

Код подтверждения недействителен.<br />
Это может быть потому, что Вы уже подтвердили свою регистрацию ранее. Если это так, то просто войдите в систему.<br />
Другие возможные причины:<br />
1. Если Вы скопировали URL из регистрационного эл. письма, пожалуйста, убедитесь, что Вы сделали это полностью и без ошибок.<br />
2. Если Вы получили подтверждающее регистрацию эл. письмо уже давно и за это время не подтвердили свою регистрацию, то, возможно, что Ваша регистрация уже не активна. В этом случае Вам необходимо зарегистрироваться снова. <br />


<?php endif; ?>
</div>
