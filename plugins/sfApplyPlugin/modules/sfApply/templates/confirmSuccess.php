﻿<div class="sf_apply_notice">
<p>
<?php if ($sf_user->getCulture() == 'lv_LV'):?>
Paldies par reģistrācijas apstiprināšanu! Jūs atrodaties portālā.
<?php else:?>
Спасибо за подтверждение регистрации! Вы находитесь на портале.
<?php endif; ?>
</p>
<p>
<?php echo button_to("Turpināt", "@homepage") ?>
</p>
</div>
