﻿<?php use_helper('I18N') ?>

<div class="sf_apply_notice">
<p>
<?php echo __('Jūsu parole ir nomainīta, un Jūs esat reģistrējies portālam.');?> 
</p>
<br />
<p>
<?php if ($sf_user->getCulture() == 'lv_LV'):?>
<?php echo button_to(__("Turpināt"), "articles/show?id=1") ?>
<?php else:?>
<?php echo button_to(__("Turpināt"), "articles/show?id=21") ?>
<?php endif;?>
</p>
</div>
