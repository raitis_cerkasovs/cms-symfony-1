﻿<?php use_helper('I18N') ?>
<div class="sf_apply_notice">
<p>
<?php echo __('Drošības apsvērumu dēļ uz e-pasta adresi, ko esat norādījis reģistrējoties, tika nosūtīta apstiprinājuma ziņa. Šajā e-pastā norādītā saite Jums jāizmanto, lai Jūs varētu nomainīt savu paroli. Ja neatrodat šo ziņu, pārbaudiet sadaļas „spam” un „bulk”.'); ?>
</p>
<p>
<?php echo __('Atvainojamies par sagādātajām neērtībām'); ?>
</p>
<p>
<br />
   <?php if ($sf_user->getCulture() == 'lv_LV'):?>
<?php echo button_to(__("Turpināt"), "@homepage") ?>
   <?php else:?>
   <?php echo button_to(__("Turpināt"), "articles/show?id=21") ?>
   <?php endif;?>

</p>
</div>
