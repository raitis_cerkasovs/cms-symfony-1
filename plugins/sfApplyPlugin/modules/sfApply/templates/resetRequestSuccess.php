﻿<?php use_helper('I18N') ?>
<?php slot('login') ?>
<?php end_slot() ?>
<?php use_helper('Validation') ?>
<?php echo form_tag('sfApply/resetRequest', array('name' => 'sf_apply_reset_request', 'id' => 'sf_apply_reset_request')) ?>
<p>
<?php echo __('Ja esat aizmirsis savu paroli, ievadiet lietotājvārdu un klikšķiniet uz „Atjaunot paroli”. Saite, kas nodrošinās paroles maiņu, tiks nosūtīta uz e-pastu, ko esat norādījis reģistrējoties.');?>
</p>
<div class="sf_apply_row">
<label for="username"><?php echo __('Lietotāja vārds');?>: </label>
<div class="sf_apply_row_content"></br />
<span style="color:#FF0000;"><?php echo form_error('username_res') ?></span>
<?php echo input_tag('username_res', $sf_params->get('username_res')) ?>
</div>
</div>
<div class="sf_apply_submit_row">
<label></label>
<?php echo submit_tag(__('Atjaunot paroli'),array('style'=>'margin-top:5px;')) ?>

</div>
</form>

