﻿<?php use_helper('I18N') ?>
<?php slot('login') ?>
<?php end_slot() ?>
<?php use_helper('Validation') ?>
<?php echo form_tag('sfApply/reset', array('name' => 'sfApplyReset', 'id' => 'sf_apply_reset')) ?>
<div class="sf_apply_notice">
<p>
<?php echo __('Paldies par savas e-pasta adreses apstiprinājumu. Paroli varat mainīt, izmantojot zemāk esošo formu.');?>
</p>
</div><br />
<div class="sf_apply_row">
<span style="color:#FF0000;"><?php echo __(form_error('password_res_suc')) ?></span>
<label for="password"><?php echo __('Jaunā parole');?>:</label>
<div class="sf_apply_row_content">
<?php echo input_password_tag('password_res_suc', $sf_params->get('password_res_suc')) ?>
</div>
</div>
<div class="sf_apply_row">
<label for="password2"><?php echo __('Atkārtot paroli');?>:</label>
<div class="sf_apply_row_content">
<?php echo input_password_tag('password2', $sf_params->get('password2')) ?>
</div>
</div>
<div class="sf_apply_submit_row">
<label></label>
<?php echo submit_tag(__('Atjaunot paroli'), array('style'=>'margin-top:5px;')) ?>
</div>
</form>

