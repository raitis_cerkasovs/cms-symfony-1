﻿<?php use_helper('I18N') ?>
<?php if ($sf_user->getCulture() == 'lv_LV'):?>
Labdien,

Paldies, ka esat reģistrējies prortālā www.mežamundarzam.lv! Drošības apsvērumu dēļ, lūdzu, izmantojiet sekojošo saiti, lai apstiprinātu savu reģistrāciju:

<?php else:?>
Здравствуйте! 

Спасибо, что зарегистрировались на портале mezamundarzam.lv! 
Из соображений безопасности, пожалуйста, нажмите на эту ссылку 
для подтверждения своей регистрации: 

<?php endif;?>

<?php echo url_for("sfApply/confirm?validate=$validate", true) ?>


