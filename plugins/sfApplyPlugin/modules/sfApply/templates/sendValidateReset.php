﻿<?php use_helper('I18N') ?>

<?php echo __('Esam saņēmuši Jūsu pieteikumu paroles maiņai. Lai to izdarītu, lūdzu, klikšķiniet uz sekojošo saiti:');?>

<?php echo url_for("sfApply/confirm?validate=$validate", true) ?>

<?php echo __('JA NEESAT SŪTĪJIS ŠĀDU PIEPRASĪJUMU, tad iespējams, ka to ir izdarījis kāds cits. Jūs varat šai ziņai nepievērst uzmanību, un Jūsu parole netiks mainīta, kamēr vien neapstiprināsiet šādu vēlmi, klikšķinot uz iepriekš norādītās saites.');?> 
