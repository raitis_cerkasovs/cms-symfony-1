<?php use_helper('Validation', 'I18N');?>

<div style="padding:10px; margin-bottom:400px;">
<fieldset>
<div id="sf_guard_auth_form">
<?php echo form_tag('@sf_guard_signin') ?>
	   <table width="auto">
	    <tr>
		 <td></td><td>
		          <div id="error">
	 	         <?php
				  echo form_error('username_');
                  ?>
				  </div>
		 </td>
		</tr>
		<tr>
		 <td style="color:#000;">
		          <?php 
                  echo label_for('username_', __('Lietotājs:'));
				  ?>
         </td><td>
		           <?php
                     echo input_tag('username_', $sf_data->get('sf_params')->get('username_'), array('style'=>'width:100px;'));
					?>
           
		   </td>
		   </tr>		   
		   	    <tr>
		 <td></td><td>
		          <div id="error">
	 	         <?php
				  echo form_error('password_');
				  ?>
				  </div>
		 </td>
		</tr>
		<tr>
		 <td style="color:#000;">
		          <?php 
                  echo label_for('password_', __('Parole:'));
				  ?>
         </td><td>
		           <?php
                     echo input_password_tag('password_', $sf_data->get('sf_params')->get('password_'), array('style'=>'width:100px;'));
					?>
           
		   </td>
		   </tr>
		   </table>
   </fieldset>

  <?php 
  echo submit_tag(__('Ienākt'));
  ?>
  </div>
﻿